<?php
  include_once('../../config/init.php');
  include_once($BASE_DIR.'database/quotes.php');
  include_once($BASE_DIR.'database/users.php');
  include_once($BASE_DIR.'database/userInf.php');
  include_once($BASE_DIR.'database/feed.php');
  include_once($BASE_DIR.'database/edit.php');
  include_once($BASE_DIR.'database/notifications.php');
  include_once($BASE_DIR.'database/profile.php');

  global $smarty;


  if (isset($_SESSION['username'])) 
  {
      assignProfileQuotes($smarty);
      assignBio($smarty);
      assignNumberOfFriends($smarty);
      assignNumberOfFollowers($smarty);
      assignNumberOfFollowing($smarty);
      assignEmail($smarty);
      assignFeedQuotes($smarty);
      $_SESSION['lastupdate'] = date('Y-m-d H:i:s');
      $smarty->display('admin/menus/manageusers.tpl');
  }
  else
  {
  	$smarty->display('home/home.tpl');
  }


  function assignFeedQuotes(&$smarty){
     $smarty->assign("feedQuotes",getQuotesFeed($_SESSION['id'])  );
  }

  function assignEmail(&$smarty){
     $smarty->assign("email",getEmailByID($_SESSION['id'])  );
  }
  function assignProfileQuotes(&$smarty) {
    $smarty->assign("profileQuotes",getQuotesByUser($_SESSION['id'])  ); /*EDITAR*/
  }

  function assignNumberOfFriends(&$smarty){
     $smarty->assign("numFriends",getNumberFriends($_SESSION['id']) );
  }
  function assignNumberOfFollowers(&$smarty){
     $smarty->assign("numFollowers",getNumberFollowers($_SESSION['id']) );   
  }
  function assignNumberOfFollowing(&$smarty){
     $smarty->assign("numFollowing",getNumberFollowing($_SESSION['id']) );   
  }
  function assignBio(&$smarty){
     $smarty->assign("bio",getBio($_SESSION['id'])  );
  }
?>
