<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/profile.php');
	include_once($BASE_DIR.'database/userInf.php');

	$result = array();
	$result['Friends']=getFriends($_SESSION['id']);
	$result['NumberFriends']=getNumberFriends($_SESSION['id']);

	echo json_encode($result);
?>