<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/profile.php');
	include_once($BASE_DIR.'database/userInf.php');

	$result = array();
	$result['Following']=getFollowing($_SESSION['id']);
	$result['NumberFollowing']=getNumberFollowing($_SESSION['id']);

	echo json_encode($result);
?>