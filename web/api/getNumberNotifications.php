<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/notifications.php');

	$result = array();

	$result['Friends']= sizeof(getFriendRequests($_SESSION['id']));
	$result['Followers']= sizeof(getNewFollowers($_SESSION['id']));
	$result['Upvotes']= sizeof(getNewUpvotes($_SESSION['id']));

	echo json_encode($result);
?>