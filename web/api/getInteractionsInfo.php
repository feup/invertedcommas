<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/userInf.php');

	$result = array();
	$result['IsFriend']=isFriend($_SESSION['id'],$_POST['idOtherUser']);
	$result['IsFollowing']=isFollowing($_SESSION['id'],$_POST['idOtherUser']);

	echo json_encode($result);
?>