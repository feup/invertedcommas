<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/notifications.php');

	$result = array();
	$result['Friends']=getFriendRequests($_SESSION['id']);
	$result['Followers']=getNewFollowers($_SESSION['id']);
	$result['Upvotes']=getNewUpvotes($_SESSION['id']);

	echo json_encode($result);
?>