<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/profile.php');
	include_once($BASE_DIR.'database/userInf.php');

	$result = array();
	$result['Followers']=getFollowers($_SESSION['id']);
	$result['NumberFollowers']=getNumberFollowers($_SESSION['id']);

	echo json_encode($result);
?>