<?php
	include_once('../config/init.php');
	include_once($BASE_DIR.'database/update.php');
	include_once($BASE_DIR.'database/userInf.php');

	
	$result = array();
	$result['ProfilePosts']=updatePostsProfile(	$_SESSION['id'],$_SESSION['lastupdate']);
	$result['FeedPosts'   ]=updatePostsFeed(	$_SESSION['id'],$_SESSION['lastupdate']);
	$result['NoFriends'   ]=getNumberFriends(   $_SESSION['id']);
	$result['NoFollowers' ]=getNumberFollowers( $_SESSION['id']);
	$result['NoFollowing' ]=getNumberFollowing( $_SESSION['id']);

	$_SESSION['lastupdate'] = date('Y-m-d H:i:s');
	
	echo json_encode($result);
	

?>