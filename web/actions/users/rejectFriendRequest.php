<?php

	include_once('../../config/init.php');
	include_once($BASE_DIR.'database/profile.php');  

	$idNotif = $_GET['notif'];

	global $conn;

	$stmt = "SELECT * FROM notificacao WHERE
			id = :idparam";
	$stmt = $conn->prepare($stmt);
	$stmt->bindParam(':idparam', $idNotif);
	$stmt->execute();
	$res = $stmt->fetch();

	$idPedAmizade = $res['idpedamizade'];

	$stmt = "SELECT * FROM pedidoamizade WHERE
			id = :idparam";
	$stmt = $conn->prepare($stmt);
	$stmt->bindParam(':idparam', $idPedAmizade);
	$stmt->execute();
	$res = $stmt->fetch();

	$idemissor = $res['idemissor'];
	$idrecetor = $res['idrecetor'];
	$estado = $res['estado'];


	$stmt = "UPDATE pedidoamizade 
	SET estado = :novoestado
	WHERE id = :idparam";

	$stmt = $conn->prepare($stmt);
	$novoEstado = "rejeitado";
	$stmt->bindParam(':novoestado', $novoEstado);
	$stmt->bindParam(':idparam', $idPedAmizade);
	$res = $stmt->execute();

	
	$result = array();
	$result['success']= $res;
	echo json_encode($result);
	
	
?>