<?php
  include_once('../../config/init.php');
  include_once($BASE_DIR.'database/users.php');   


  $smarty = new Smarty;

  if (!$_POST['loginusername'] || !$_POST['loginpassword']) {
    $_SESSION['error_messages'][] = 'Invalid login';
    $_SESSION['form_values'] = $_POST;
    
    exit;
  }

  $username = $_POST['loginusername'];
  $password = $_POST['loginpassword'];
  
  if (isLoginCorrect($username, $password)){
    $_SESSION['username'] = $username;
    $idUser = getIdByUser($username);
    $_SESSION['id'] = $idUser;
    $_SESSION['success_messages'][] = 'Login successful'; 

  } else {
    $_SESSION['error_messages'][] = 'Login failed';  
  }
  header("Location: $BASE_URL");
?>
