<?php
  include_once('../../config/init.php');
  include_once($BASE_DIR .'database/users.php');  

  if ( !$_POST['username'] || !$_POST['password'] || !$_POST['email'] || !$_POST['confirmpassword'] || !$_POST['date']) 
  {
    $_SESSION['error_messages'][] = 'All fields are mandatory';
    $_SESSION['form_values'] = $_POST;
    header("Location: $BASE_URL");
   
    exit;
  }

  

  $username = strip_tags($_POST['username']);
  $password = $_POST['password'];
  $confpassword = $_POST['confirmpassword'];
  $email = $_POST['email'];
  $date = $_POST['date'];

  try {
    createUser($username, $password, $confpassword, $email, $date);

  } catch (PDOException $e) {
  
    //echo $e->getMessage();
    $_SESSION['error_messages'][] = $e->getMessage();
    header("Location: $BASE_URL" . "pages/home/home.php#!register");
    exit;
  }
  $_SESSION['success_messages'][] = 'User registered successfully';  
  header("Location: $BASE_URL");
  //$smarty->display('home/home.tpl');
?>
