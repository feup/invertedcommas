<?php /* Smarty version Smarty-3.1.15, created on 2014-06-08 17:30:43
         compiled from "/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/menus/manageusers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1164807709538e13b082a209-72658327%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '43604b60a81c8d409b4ac137ec22d7725bfc066b' => 
    array (
      0 => '/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/menus/manageusers.tpl',
      1 => 1402245038,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1164807709538e13b082a209-72658327',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_538e13b08ab378_32330506',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'USERNAME' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538e13b08ab378_32330506')) {function content_538e13b08ab378_32330506($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('admin/common/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
images/admin/logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><a href="#"><i class="fa fa-bar-chart-o"></i> Manage Users</a></li>
            <li><a href="managetickets.php"><i class="fa fa-ticket"></i> Manage Tickets</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Website Settings <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">General Settings</a></li>
                <li><a href="#">Maintenance Mode</a></li>
                <li><a href="#">Spam Control</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <!--li class="dropdown messages-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Tickets <span class="badge">7</span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">7 New Tickets</li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="http://placehold.it/50x50"></span>
                    <span class="name">John Smith:</span>
                    <span class="message">Hey there, I wanted to ask you something...</span>
                    <span class="time"><i class="fa fa-clock-o"></i> 4:34 PM</span>
                  </a>
                </li>
                <li class="divider"></li>
                <li><a href="#">View Inbox <span class="badge">7</span></a></li>
              </ul>
            </li-->
            
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['USERNAME']->value, ENT_QUOTES, 'UTF-8', true);?>
 <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
/pages/home/home.php#!profile"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><i class="fa fa-globe"></i> Visit Website </a></li>
                <li class="divider"></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
/actions/users/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

  <div id="page-wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h1>Manage Users <small>Edit/Delete Users and their content</small></h1>
        <ol class="breadcrumb">
          <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active"><i class="fa fa-table"></i> Manage Users</li>
        </ol>
        <div class="alert alert-info alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Add, edit and remove users and user content. 
        </div>
      </div>
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-12">

        <h2>Latest Users</h2>
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped tablesorter paginated">
            <thead>
              <tr>
                <th class="header">User ID <i class="fa fa-sort"></i></th>
                <th class="header">Username <i class="fa fa-sort"></i></th>
                <th class="header">Total Quotes <i class="fa fa-sort"></i></th>
                <th class="header">View</i></th>
              </tr>
            </thead>
            <tbody id="recentusers">
              
            </tbody>
          </table>
        </div>


      </div>
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-12">
    <form role="form" id="searchadminform" >
      <div class="form-group">
        <label> Search Content </label>
        <input type="text" class="form-control" id="sc" name="search_content" style="width:30%;" pattern="^(0|[1-9][0-9]*)$" oninvalid="this.setCustomValidity('Enter a numeric value.')">
      </div>

      <div class="form-group" style="float:left">
        <label> Search by </label>
        <select name="searchby" id="sb" style="width: 100px;padding-bottom: 2px;box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);border: 1px solid #ccc;border-radius: 4px;">
          <option value="0">User ID</option>
          <option value="1">Quote ID</option>
        </select>
      </div>
      <button type="submit" style="margin-bottom: 20px;padding-bottom: 0px;background: #ddd;border: 1px solid #ccc;border-radius: 4px;margin-left: 6px;"><i class="fa fa-search"></i></button>
    </form>

    

    <div class="table-responsive" id="searchresultstable">
      
    </div>
  </div>
</div><!-- /.row -->





<?php echo $_smarty_tpl->getSubTemplate ('admin/common/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
