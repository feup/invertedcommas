<?php /* Smarty version Smarty-3.1.15, created on 2014-06-06 19:34:41
         compiled from "/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/users/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1626297915358eaf91c2ac7-85339563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c97d12af773efa1d3e50f196b7d4cc7789b822ee' => 
    array (
      0 => '/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/users/settings.tpl',
      1 => 1402079679,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1626297915358eaf91c2ac7-85339563',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5358eaf91ca224_10073985',
  'variables' => 
  array (
    'USERNAME' => 0,
    'email' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5358eaf91ca224_10073985')) {function content_5358eaf91ca224_10073985($_smarty_tpl) {?>


<div data-id="!settings" class="contentWrapper contactPage darkStyle"  >



	<div class="m-Scrollbar">  

		<hr class="alignToMiddle" /> 

		<div class="container-fluid">     	
			<div class="row-fluid">
				<div class="span12" > 
					<div class="heading_stroke"> 
						<div class="heading_stroke_wrapper">
							<div class="stroke-text"><h2>Edit your settings</h2></div>
							<div class="stroke-holder"><div class="stroke-line"></div></div>
						</div>
					</div>
				</div>               
			</div>        
		</div>

		<div class="container-fluid darkStyle desktop_alignLeft" > 

			<div class="row-fluid" >
				    <dl class="accordion" data-autohide="true" data-openfirstelement="true">
                  
                      <!-- Accordion tab -->
                      <dt>
                          <a href="" class="normal"><span class="closeOpen" style="background-position: 0px -15px;"></span><span class="acc_heading">
                              <!-- Heading -->
                              Account Settings
                          </span></a>
                      </dt>
                      
                          <dd class="active" style="display: block;">
                             <div class="span12"> 
        <ul class="nav nav-tabs">
                  <li class="active" ><a href="#tab_y1" data-toggle="tab">Change Username</a></li>
                  <li class=""><a href="#tab_y2" data-toggle="tab">Change Password</a></li>
                  <li class=""><a href="#tab_y3" data-toggle="tab">Change Email</a></li>
          </ul>
                <div class="tab-content">
                  <div class="tab-pane fade active in" id="tab_y1">
                      <form name="register " method="get" action="../../actions/users/edit.php" >
                      <div class="settingstag span3" > 
                        <span>Current Username: </span>
                        </div>
                        <span class="label label-info span3 settingslabel" ><?php echo $_smarty_tpl->tpl_vars['USERNAME']->value;?>
</span><br><br>

<div class="settingstag span3"> 
                        <span>New Username: </span>
                        </div>
                        <input type="text" value="" id="newUsername" name="newUsername" class="span3 settingsinput"  />
<br><br>

<div class="settingstag span3" > 
                        <span>Password: </span>
                        </div>
                        <input type="password" value="" id="oldPassword" name="oldPassword" class="span3 settingsinput"  />
<br><br>
<button type="submit" id="settingssubmit"  class="button transparent alignLeft settingssubmit" style="background-color:#5cb85c!important;">Confirm Changes</button>

                      </form>
                  </div>

                   <div class="tab-pane fade" id="tab_y2">
                      <form name="register " method="get" action="../../actions/users/edit.php" >
                      <div class="settingstag span3"> 
                        <span>New Password: </span>
                        </div>
                        <input type="text"  value="" id="newPassword" name="newPassword" class="span3 settingsinput"  /><br>

<div class="settingstag span3"> 
                        <span>Confirm Password: </span>
                        </div>
                        <input type="text"  value="" id="newPassword" name="newPassword" class="span3 settingsinput"  />
<br><br>

<div class="settingstag span3"> 
                        <span>Old Password: </span>
                        </div>
                        <input type="text" value="" id="oldPassword" name="oldPassword" class="span3 settingsinput"  />
<br><br>
<button type="submit" id="settingssubmit"  class="button transparent alignLeft settingssubmit" style="background-color:#5cb85c!important;">Confirm Changes</button>

                      </form>
                  </div>

                  <div class="tab-pane fade" id="tab_y3">
                      <form name="register " method="get" action="../../actions/users/edit.php" >
                      <div class="settingstag span3" > 
                        <span>Current Email: </span>
                        </div>
                        <span class="label label-info span3 settingslabel"><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</span><br><br>

<div class="settingstag span3"> 
                        <span>New Email: </span>
                        </div>
                        <input type="text" value="" id="newEmail" name="newEmail" class="settingsinput span3"  />
<br><br>

<div class="settingstag span3"> 
                        <span>Password: </span>
                        </div>
                        <input type="text" value="" id="oldPassword" name="oldPassword" class="span3 settingsinput"  />
<br><br>
<button type="submit" id="settingssubmit"  class="button transparent alignLeft settingssubmit" style="background-color:#5cb85c!important;">Confirm Changes</button>

                      </form>
                  </div>
                </div>
                <br>
            </div>
                          </dd>
                          
                          
                      <!-- Accordion tab -->
                      
                      
                      
                      <!-- Accordion tab -->
                      <dt>
                          <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading">
                              <!-- Heading -->
                              Privacy Settings
                          </span></a>
                      </dt>
                          <dd style="display: none;">
                              <span class="acc_content" style="width: 100%;">
                                  <!-- Tab content -->
                                  <div class="span4 settingstag" style="height: 126px;">
                                  <div class="settingstag privacy">Profile</div>
 <form id="profilePrivacyForm" method="get" action="../../actions/users/edit.php">                                 
<tbody>
<tr>
<td>
<input type="radio" name="profile_privacy" id="radio1" class="css-checkbox" checked="checked" value="1">
<label for="radio1" class="css-label cb0">Visible to anyone</label>
</td>
<tr>
<td>
<input type="radio" name="profile_privacy" id="radio2" class="css-checkbox" value="0">
<label for="radio2" class="css-label cb0">Visible only to friends</label>
</td>
</tr>
</tbody>
</div>

                                  <div class="span4 settingstag">
                                  <div class="settingstag privacy">Friends</div>
                                  <tbody>
<tr>
<td>
<input type="radio" name="friends_privacy" id="radio3" class="css-checkbox" value="1">
<label for="radio3" class="css-label cb0">Visible to anyone</label>
</td>
<tr>
<td>
<input type="radio" name="friends_privacy" id="radio4" class="css-checkbox" checked="checked" value="0">
<label for="radio4" class="css-label cb0">Visible only to friends</label>
</td>
</tr>
</tbody>
</div>
                                  <div class="span4 settingstag">
                                  <div class="settingstag privacy">Followers</div>
                                  <tbody>
<tr>
<td>
<input type="radio" name="followers_privacy" id="radio5" class="css-checkbox" checked="checked" value="1">
<label for="radio5" class="css-label cb0">Visible to anyone</label>
</td>
<tr>
<td>
<input type="radio" name="followers_privacy" id="radio6" class="css-checkbox" value="0">
<label for="radio6" class="css-label cb0">Visible only to friends</label>
</td>
</tr>
</tbody>
</div>

<div class="span4"></div>
<div class="span4"><br><button type="submit" id="privacysubmit"  class="button transparent alignLeft settingssubmit" style="background-color:#5cb85c!important;">Confirm Changes</button></form></div>

                              </span>
                          </dd>
                          
                  </dl>                  

            </div>
            
            
            <hr class="separator_mini">


        </div>           

    </div>
</div>
<?php }} ?>
