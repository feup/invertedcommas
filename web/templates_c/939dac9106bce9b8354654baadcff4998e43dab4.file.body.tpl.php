<?php /* Smarty version Smarty-3.1.15, created on 2014-06-03 19:09:35
         compiled from "/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/common/body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1625800345538ca2c680b4f7-03094243%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '939dac9106bce9b8354654baadcff4998e43dab4' => 
    array (
      0 => '/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/common/body.tpl',
      1 => 1401818973,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1625800345538ca2c680b4f7-03094243',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_538ca2c6817c43_71431125',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538ca2c6817c43_71431125')) {function content_538ca2c6817c43_71431125($_smarty_tpl) {?>
 <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Dashboard <small>Statistics Overview</small></h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
            </ol>
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Welcome to <a class="alert-link" href="http://invertedcommas.com">InvertedCommas</a> Control Panel!
            </div>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-comments fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">14</p>
                    <p class="announcement-text"> Support Tickets Received :)</p>
                  </div>
                </div>
              </div>
              <!--a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      View New Support Tickets
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a-->
            </div>
          </div>
          <div class="col-lg-3">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-check fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">12</p>
                    <p class="announcement-text">Tickets marked as Solved :D</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <!--div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      View Progress in Problem Resolving
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div-->
              </a>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="panel panel-danger">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">2</p>
                    <p class="announcement-text">Tickets marked as Unsolved :(</p>
                  </div>
                </div>
              </div>
              <a href="#">
                <!--div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Manage Error/Bug Assignment
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div-->
              </a>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="panel panel-info" style="margin-bottom: 0px;margin-top: 18px;">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                  </div>
                  <div class="col-xs-6 text-right">
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Manage Tickets
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                  </div>
                  <div class="col-xs-6 text-right">
                  </div>
                </div>
              </div>
              </div>
          </div>
          
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Last Month's Traffic Statistics: May 1, 2014 - May 31, 2014</h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-area"></div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

        <div class="row">
          <div class="col-lg-4">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Goncalo's Social Character Traits (sondage): March 24, 2014</h3>
              </div>
              <div class="panel-body">
                <div id="morris-chart-donut"></div>
                <div class="text-right">
                  <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o"></i> User Stories Awaiting Implementation</h3>
              </div>
              <div class="panel-body">
                <div class="list-group">

    <div id="trellonot">

    </div>

 
                </div>
                <div class="text-right">
                  <a href="https://trello.com/b/cfHuOiBM/lbaw">View Trello's Page <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o"></i> Last User Stories Implemented</h3>
              </div>
              <div class="panel-body">
                <div class="list-group">

    <div id="trelloyes">

    </div>

 
                </div>
                <div class="text-right">
                  <a href="https://trello.com/b/cfHuOiBM/lbaw">View Trello's Page <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper --><?php }} ?>
