<?php /* Smarty version Smarty-3.1.15, created on 2014-06-08 17:30:40
         compiled from "/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/menus/managetickets.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1179718235391f3380bab54-78168322%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca78e88050903acf587e475b886cc5c61df8b9b8' => 
    array (
      0 => '/opt/lbaw/lbaw1321/public_html/ic/frmk/templates/admin/menus/managetickets.tpl',
      1 => 1402245024,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1179718235391f3380bab54-78168322',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5391f33810e193_05436011',
  'variables' => 
  array (
    'BASE_URL' => 0,
    'USERNAME' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5391f33810e193_05436011')) {function content_5391f33810e193_05436011($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('admin/common/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
images/admin/logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="manageusers.php"><i class="fa fa-bar-chart-o"></i> Manage Users</a></li>
            <li class="active"><a href="#"><i class="fa fa-ticket"></i> Manage Tickets</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Website Settings <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">General Settings</a></li>
                <li><a href="#">Maintenance Mode</a></li>
                <li><a href="#">Spam Control</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['USERNAME']->value, ENT_QUOTES, 'UTF-8', true);?>
 <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
/pages/home/home.php#!profile"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
"><i class="fa fa-globe"></i> Visit Website </a></li>
                <li class="divider"></li>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['BASE_URL']->value;?>
/actions/users/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

  <div id="page-wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h1>Manage Tickets </h1>
      </div>
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-12">

        <h2>Tickets</h2>
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped tablesorter paginated">
            <thead>
              <tr>
                <th class="header">User ID <i class="fa fa-sort"></i></th>
                <th class="header">Username <i class="fa fa-sort"></i></th>
                <th class="header">Email <i class="fa fa-sort"></i></th>
                <th class="header">Estado <i class="fa fa-sort"></i></th>
              </tr>
            </thead>
            <tbody id="tickets">
              
            </tbody>
          </table>
        </div>
      </div>
    </div><!-- /.row -->
  </div>
</div><!-- /.row -->





<?php echo $_smarty_tpl->getSubTemplate ('admin/common/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
