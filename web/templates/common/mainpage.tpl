<div class="show_notificationsFriends">
{include file='nav/show_notifications.tpl'}
</div>

<div class="show_notificationsFollowers">
{include file='nav/show_notifications.tpl'}
</div>

<div class="show_notificationsUpvotes">
{include file='nav/show_notifications.tpl'}
</div>


<div class="notifications">
{include file='nav/notifications.tpl'}
</div>

  <div class="home_container">
  <div class="home_page left_content darkStyle">
    <div id="borderRight"> </div>
        <!--
          Logo place holder
        -->
        <div class="logo_holder" >
          <div class="logo center_position" >
            <a href="!home">
              <img class="logo_normal" src="{$BASE_URL}images/logo_ic_white2.png" data-retina="yes" alt="logo text">
            </a>
          </div>
        </div>

        <div class="container-fluid left_Holder">
  
          <hr class="top_space">
      
                <div class="row-fluid" id="userinfo">
                <div class="span12"  > 
                

                  <h7 class="upperCase"><span style="opacity:0.5;">Signed in as</span>  {$USERNAME|escape}</h7>
                  <br>
                  <h7 class="upperCase"><span style="opacity:0.5;">Friends |</span> {$numFriends}</h7>
                  <br>
                  <h7 class="upperCase"><span style="opacity:0.5;">Followers |</span> {$numFollowers}</h7>
                  <br>
                  <h7 class="upperCase" style="margin-right: 3px;"><span style="opacity:0.5;">Following |</span> {$numFollowing}</h7>
                </div>
              </div>
<hr class="separator_mini">
              <div class="row-fluid animate" >
                  <div class="span12">

<dl class="accordion" data-autohide="false" data-openfirstelement="false" id="addquoteacc" >
                
                    <!-- Accordion tab -->
                    <dt>
                        <a href="" class="normal"><span class="closeOpen" style="background-position: 0px -15px;"></span><span class="acc_heading" style="  background-color:rgba(75, 72, 72, 0.6)!important;border-radius: 5px!important;text-align:center;width: 242px;;margin-top: 22px;" >
                            <!-- Heading -->
                            ADD A QUOTE
                        </span></a>
                    </dt>
                        <dd class="active" style="display: block;">
                            <span class="acc_content">
                                <!-- Tab content -->
                        <div class="join_form" style="text-align:left;">
                          <table>
                            <tr>
                              <td>
                                <form id="addquoteform" method="get">
                                  Quote:<br><textarea type="text" value="" style="" id="addquote" name="addquote"   rows="3"  ></textarea>
                                  Source/Author:<br><input type="text" value="" id="addsource" name="addsource" /><br>
                              </td>
                              <td>
                                <button type="submit" id="quote_submit" style="height: 125px;margin-top: 12px;"> <i class="fa fa-plus-circle fa-2x"></i>
                                </button> 
                              </td>
                                </form>
                            </tr>
                          </table>


                      <!--form name="joinus" method="post" >
                        <input type="text" value="Search..." id="joinus_email" name="joinus_email"  />
                        <button type="submit" id="joinus_submit" style="margin-top: 72px;margin-left: 84%;"> <img src="images/loopa.png"></button> 
                      </form-->
                  </div>
                            </span>
                        </dd>
                                               



                    </dl>

              </div>
              </div>

            <br>
            <hr class="separator_mini">
            <hr class="separator_mini">
            
        </div>
        
    </div>


    <div class="home_page right_content darkStyle ">   

      <div class="container-fluid right_Holder">

        <hr class="top_space">

        <a class="page_close"> </a>

        <div class="row-fluid" id="stayconnected">
          <div class="span12">
            
          </div>
        </div>

        <div class="row-fluid" id="stayconnected">
          <div class="span12">
            <div class="t3_space">
              <h6></h6>
            </div>
          </div>
        </div>

        <div class="row-fluid">
          <div class="span12 menu_holder">

                    <!--
                      Page Navigation Area
                    -->
                    <div class="header " >

                      <div class="header_content" >
 
                              {include file='nav/navloggedin.tpl'}

                      </div>

                    </div> 

                </div>

            </div> 



            <hr class="separator_mini">

        </div>




    </div>
</div>  

<div class="bodyContainer" >

{include file='users/newsfeed.tpl'}
{include file='users/profile.tpl'}
{include file='users/settings.tpl'}
{include file='others/aboutus.tpl'}
{include file='others/help.tpl'}
{include file='users/foreignProfile.tpl'}
{include file='others/promo.tpl'}
{include file='others/searchresults.tpl'}


</div>