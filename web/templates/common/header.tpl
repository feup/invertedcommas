<!DOCTYPE html>
<html>
  <head>
    <title>InvertedCommas - Share your Quotes</title>
    <meta charset='utf-8'>

<!-- Mobile Specific Metas
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta property="og:title" content="InvertedCommas" />
  <meta property="og:site_name" content="InvertedCommas">
  <meta name="description" content="Share your Quotes">
  <meta property="og:description" content="Share your Quotes" />
  <meta property="og:image" content="http://www.gravatar.com/avatar/{$quote.Email|md5}?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" />

  <!-- Favicons
  ================================================== -->
  <link rel="shortcut icon" href="{$BASE_URL}images/icons/favicon.ico">
  <link rel="apple-touch-icon" href="{$BASE_URL}images/icons/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{$BASE_URL}images/icons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="{$BASE_URL}images/icons/apple-touch-icon-114x114.png">

  <!-- Included CSS files 
  ================================================== -->
    
  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:200,100' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/pages/feed.css" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/pages/notifications.css" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/pages/profile.css" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/pages/settings.css" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/pages/search.css" />

    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/bootstrap-responsive.min.css">    
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}font-awesome/css/font-awesome.min.css">    
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/main.min.css">
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/base.min.css">
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/color-dark.css"> 
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/color-light.css">
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/jquery.mCustomScrollbar.css" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/magnific-popup.css" media="screen" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/supersized.min.css" media="screen" />
    <link rel="stylesheet" type="text/css"  href="{$BASE_URL}css/supersized.shutter.min.css" media="screen" />
    
    

   <!-- Included javascript files 
    ================================================== -->
    
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.fitvids.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> 
    <script type="text/javascript" src="{$BASE_URL}javascript/supersized.3.2.7.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/supersized.shutter.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.magnific-popup.min.js"></script>   
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.cycle.all.js"></script>     
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.nicescroll.min.js"></script>      
    <script type="text/javascript" src="{$BASE_URL}javascript/custom.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.comingsoon.min.js"></script>   
    <script type="text/javascript" src="{$BASE_URL}javascript/main-fm.min.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/main-fm.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/jquery.jstwitter.js"></script> 
    <script type="text/javascript" src="{$BASE_URL}javascript/md5.js"></script> 

    <script type="text/javascript" src="{$BASE_URL}javascript/clearFieldsRegistryForm.js"></script> 
    <script type="text/javascript" src="{$BASE_URL}javascript/clearFieldsLoginForm.js"></script> 
    <script type="text/javascript" src="{$BASE_URL}javascript/clearFieldsLeftHolder.js"></script> 
    
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/profile.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/quotes.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/search.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/settings.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/socialshare.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/feed.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/notification.js"></script>
    <script type="text/javascript" src="{$BASE_URL}javascript/pages/foreignProfile.js"></script>
    

<link href="//vjs.zencdn.net/4.6/video-js.css" rel="stylesheet">
<script src="//vjs.zencdn.net/4.6/video.js"></script>

    <script type="text/javascript">


      /* Initialize supersized fullscreen image gallery */
      function superGalleryInit (){     

        jQuery(function($){
          $.supersized({
          slideshow               :   1,      // Slideshow on/off
          autoplay        : 1,      // Slideshow starts playing automatically
          slide_interval          :   1000,   // Length between transitions       

          slides  :   [ // Slideshow Images, image_small attribute is used to load the mobile version image, main_title attribute is used to add the image tile div

          { image : '{$BASE_URL}images/home_slider/suica_bg.jpg', image_small : '{$BASE_URL}images/home_slider/suica_bg.jpg', slide_interval: 9000},
          { image : '{$BASE_URL}images/home_slider/image00.jpg', image_small : '{$BASE_URL}images/home_slider/image00.jpg', slide_interval: 9000},
          { image : '{$BASE_URL}images/home_slider/trees.jpg', image_small : '{$BASE_URL}images/home_slider/trees.jpg', slide_interval: 9000},
          { image : '{$BASE_URL}images/home_slider/pradaria.jpg', image_small : '{$BASE_URL}images/home_slider/pradaria.jpg', slide_interval: 9000}
          

         
          ]
        });
        }); 

}

</script>



</head>

    <div id="error_messages">
    {foreach $ERROR_MESSAGES as $error}
      <div class="alert alert-danger">  <strong>{$error}</strong>  Inverted Commas Loves You</div>
    {/foreach}
    </div>
    <div id="success_messages">
    {foreach $SUCCESS_MESSAGES as $success}
      <div class="success"><div class="alert alert-success"> <strong>{$success}</strong> Inverted Commas Loves You</div>
      </div>
    {/foreach}
</div>





  <body>


      <!--  site loading -->
      <div class="pageFade" > </div>


<!--
  Mobile Header
-->

<div class="mobile_header">
  <div class="logo_holder">
  </div>

  <div class="showHideMenu">
    <i class="fa fa-bars"></i>
  </div>

  <div class="menu_holder">
  </div>

</div>   