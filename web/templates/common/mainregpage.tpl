

  <div class="home_container">
  <div class="home_page left_content darkStyle">
    <div id="borderRight"> </div>
        <!--
          Logo place holder
        -->
        <div class="logo_holder" >
          <div class="logo center_position" >
            <a href="!home">
              <img class="logo_normal" src="{$BASE_URL}images/logo_ic_white2.png" data-retina="yes" alt="logo text">
            </a>
          </div>
        </div>

        <div class="container-fluid left_Holder" id="welcome">

          <hr class="top_space" >
      
                <div class="row-fluid">
                    <div class="span12"  >                  
                        <h1 class="upperCase" >Welcome</h1>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12" >
                        <span class="font_medium_x t2_space" >The best way to share that one quote</span>
                    </div>
                    <hr>
                </div>



            <br>
            <hr class="separator_mini">
            

        </div>
        
    </div>


    <div class="home_page right_content darkStyle ">   

      <div class="container-fluid right_Holder" >

        <hr class="top_space">

        <a class="page_close"> </a>

        <div class="row-fluid">
          <div class="span12">
            <h4 class="t_space" id="stayconnected">Stay Connected</h4>
          </div>
        </div>

        <div class="row-fluid">
          <div class="span12">
            <div class="t3_space" >
              <h6 id="stayconnected">...It's free and always will be

              </h6>
            </div>
          </div>
        </div>

        <div class="row-fluid">
          <div class="span12 menu_holder">

                    <!--
                      Page Navigation Area
                    -->
                    <div class="header " >

                      <div class="header_content" >



                      
                       
                              {include file='nav/navloggedout.tpl'}
                     
                         
                          




                      </div>

                    </div> 

                </div>

            </div> 



            <hr class="separator_mini">

        </div>




    </div>
</div>  


<div class="bodyContainer" >

{include file='users/login.tpl'}
{include file='users/register.tpl'}
{include file='others/aboutus.tpl'}
{include file='others/promo.tpl'}
{include file='others/help.tpl'}

</div>