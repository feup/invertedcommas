<div data-id="!feed" class="contentWrapper contactPage darkStyle" >

	<div class="m-Scrollbar">

		<hr class="alignToMiddle" />

		<div class="container-fluid">     	
			<div class="row-fluid">
				<div class="span12" > 
					<div class="heading_stroke"> 
						<div class="heading_stroke_wrapper">
							<div class="stroke-text"><h2>Feed</h2></div>
							<div class="stroke-holder"><div class="stroke-line"></div></div>
						</div>
					</div>
				</div>               
			</div>
		</div>  
		
		<div id="completepage">
		<!-- Tab section -->
			<script>feedupdate(); </script>	
		</div>
	</div>

</div>