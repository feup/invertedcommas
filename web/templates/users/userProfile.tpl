
    <div class="m-Scrollbar">

        <hr class="alignToMiddle" />

        <div class="container-fluid">       
            <div class="row-fluid">
                <div class="span12"> 
                    <div class="heading_stroke"> 
                        <div class="heading_stroke_wrapper">
                            <div class="stroke-text"><h2>{$username}'s Profile</h2></div>
                            <div class="stroke-holder"><!--div class="stroke-line" style="width: 80%;"></div--></div>
                            <div id="userInteraction">
                                <script> userInteractionUpdate()</script>
                                <!--<button class="addfriend" id="btn_addfriend"><i class="fa fa-users"></i> Add Friend</button>
                                <button class="followuser" id="btn_followuser"><i class="fa fa-eye"></i> Follow</button>-->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="description">
                <div class="span3" id="profilepic">
                <div class="thumbnail_list" style="width:170px" >                   
                <div class="edit_pic_button" onclick="click('+"'profilepic'"+')"><i class="fa fa-pencil-square-o"></i></div>
                                <a class="magnificPopup imgBorder_inner" data-title="<span style=&quot;position:absolute&quot;>Upload new picture:</span>" href="{$BASE_URL}images/avatars/default.png" target="_self">
                                    <img src="http://www.gravatar.com/avatar/{$email|md5}?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic">
                                </a>        
                </div>
                </div>
                <div class="span9">
                <h5>Personal Bio
                </h5>
                <p>{$bio}
                </p>
                </div>
                </div>               
            </div>
        </div> 
        <br>

        <div id = "completepage" class="container-fluid">   

            <div class="row-fluid">
                <div class="span12" > 
                    <dl class="accordion" data-autohide="false" data-openfirstelement="true" >
                
                    <!-- Accordion tab -->
                    <dt>
                        <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading">
                            <!-- Heading -->
                            User Quotes
                        </span></a>
                    </dt>
                        <dd style="display: none;" class="">
                        <div id="profilequotes">
                            <script> profileupdate(); </script>
                        </div>

</dd>     


                    <!-- Accordion tab -->
                    <dt>
                        <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading" id="friendsDropDown">
                            <!-- Heading -->
                            Friends Info
                        </span></a>
                    </dt>
                        <dd style="display: none;" class="">
                        <div class="row-fluid">
                            <span class="acc_content">

<div class="span12"> 
        <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_bottom_friends"  id="traf" data-toggle="tab">Friends ({$numFriends})</a></li>
                  <li class=""><a href="#tab_bottom_followers" id="traf2" data-toggle="tab">Followers ({$numFollowers})</a></li>
                  <li class=""><a href="#tab_bottom_following" id="traf3" data-toggle="tab">Folowing ({$numFollowing})</a></li>
          </ul>
                <div class="tab-content">
                  <div class="tab-pane fade active in" id="tab_bottom_friends"> 
                        <script> friendsUpdate() </script>
                  </div>
                
                  <div class="tab-pane fade active in" id="tab_bottom_followers">
                        <script> followersUpdate() </script>
                  </div>
                    
                  <div class="tab-pane fade active in" id="tab_bottom_following">
                        <script> followingUpdate() </script>
                  </div>
                
                </div>

                <br>
            </div>

                            </span>
                            </div>
                        </dd>   





                </dl>







                </div>

                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


    </div>

</div>
</div>