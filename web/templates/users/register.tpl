<div data-id="!register" class="contentWrapper contactPage darkStyle"  >

  <div class="m-Scrollbar">  

    <hr class="alignToMiddle" /> 

    <div class="container-fluid">       
      <div class="row-fluid">
        <div class="span12" > 
          <div class="heading_stroke"> 
            <div class="heading_stroke_wrapper">
              <div class="stroke-text"><h2>Register</h2></div>
              <div class="stroke-holder"><div class="stroke-line"></div></div>
            </div>
          </div>
        </div>               
      </div>        
    </div>

    <div class="container-fluid darkStyle desktop_alignLeft" > 

            
            <div class="row-fluid" >


              <div class="span12" >
                
                <div class="row-fluid transparent">
                  <!--Email contact Form-->
                  <form name="register " method="post" action="../../actions/users/register.php" >

                    
                    <div class="row-fluid">
                      <div class="span6">
                        <input type="text" value="Username" id="username" name="username" class="span12"  />
                      </div>
                    </div>  
                    <div class="row-fluid">
                      <div class="span6">
                        <input type="text" value="Password" id="password" name="password" class="span12"   />
                      </div>

                      <div class="span6">
                        <input type="text" value="Confirm Password" id="confirmpassword" name="confirmpassword" class="span12"   />
                      </div>
                      </div>
                    <div class="row-fluid">

                      <div class="span12">
                        <input type="email" value="Email" id="email" name="email" class="span12"  />
                      </div>
                    </div>

                    <div class="row-fluid">      
                    <div class="span6">              
                         <span class="label label-inverse span12" style="padding-top: 7px">Date of Birth</span>
                    </div>
                    <div class="span6">     
                         <input class= "span12" type="date" value="Data de Nascimento" id="date" name="date"   />
                    </div>
                    </div>


                    </div>
                    
                    <div class="row-fluid">
                      <div class="span6">
                        <button type="submit" id="email_submit"  class="button transparent alignLeft" style="background-color:#5cb85c!important;">Sign up</button>                                 
                        <div id="reply_message" ></div> 
                      </div>
                    </div>
                  </form>
                </div>

              </div> 
            </div>
            
            <hr class="separator_mini">


        </div>           

    </div>
