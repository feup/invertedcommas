<div data-id="!help" class="contentWrapper darkStyle" >

  <div class="m-Scrollbar"> 

    <hr class="alignToMiddle" />  

    <div class="container-fluid">       
      <div class="row-fluid">
        <div class="span12" > 
          <div class="heading_stroke"> 
            <div class="heading_stroke_wrapper">
              <div class="stroke-text"><h2>Help </h2></div>
              <div class="stroke-holder"><div class="stroke-line"></div></div>
            </div>
          </div>
        </div>               
      </div>
      

      <!-- Tab section -->

      <div class="row-fluid">            
        <div class="span12">Are you a prestigious author? A quotes enthusiast? Are you just hanging around? This page is dedicated to all of you, all of InvertedCommas community.
          <br><br><i>"We can't help everyone, but everyone can help someone."</i> <br><span style="opacity: 0.5;">--Ronald Reagan</span><br><br>We've gathered in this page all the information you need to enhance your quoting experience.<br>
          Join the good guys, help us improve, ask us anything. We're here to help.
          <br><br><br></div>    
        </div>

        <div class="row-fluid">            
          <div class="span6"> 
            <a class="services right"> 
              <div class="description">                
                <h5 class="upperCase">Am I quoting this right? </h5>
                <p>InvertedCommas provides a site navigation tutorial to new and old users.</p>
              </div>
              <span class="service_icon"><img src="{$BASE_URL}images/help/tutorial.png" alt="image01"></span>
            </a>
            <hr class="separator">
          </div>

          <div class="span6"> 
            <a class="services"> 
              <span class="service_icon"><img src="{$BASE_URL}images/help/faq.png" alt="image02"></span>
              <div class="description">                
                <h5 class="upperCase">FAQ </h5>
                <p>Frequently asked questions. That's about it. Check it before anything else, it might be worth your while. </p>
              </div>
            </a>
            <hr class="separator">
          </div>                            
        </div>
        <dl class="accordion" data-autohide="true" data-openfirstelement="true">

          <!-- Accordion tab -->
          <dt>
            <a href="" class="normal"><span class="closeOpen" style="background-position: 0px -15px;"></span><span class="acc_heading help1"  style="position: absolute;margin-top: -160px;height: 115px;opacity: 0.01;width: 310px;">
              <!-- Heading -->
               
            </span></a>
          </dt>

          <dd class="active" style="display: block;">
            <span class="acc_content" style="z-index:2">
              <h5>Adding a new quote</h5>
              <div class="settingstag span6" > 
                        <span>Press the button on the left that says "ADD A QUOTE". Fill the fields and submit. </span>
                        </div><br><br>
              <h5>Searching content</h5>
              <div class="settingstag span6" > 
                        <span>Start typing on the input field on the right side of the page. Instructions about how to use our search engine will automatically show. </span>
                        </div><br><br><br>
              <h5>Adding/Following users</h5>
              <div class="settingstag span6" > 
                        <span>Whenever you see a profile picture you can click it to access the respective profile page. In there, on the top, you can find the add and follow buttons.</span>
                        </div>
              </span>
            </dd>


            <!-- Accordion tab -->
            <dt>
              <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading help2" style="position: absolute;margin-top: -484px;margin-left: 367px;height: 458px;z-index: 1;opacity: 0.01;width: 359px;">
                <!-- Heading -->
                 
              </span></a>
            </dt>
            <dd style="display: none;">
              <span class="acc_content" style="z-index:2"><ol>
               <li>
                 <span class="label label-success" style="width: 93%;">Can I insert a quote of my own?</span><br>
                 Sure thing. Just make sure you mention yourself as the author!<br></li>

                 <li><span class="label label-success" style="width: 93%;">How many girls are there in the team?</span><br>
                  Unfortunately Leonel is the only girl in our team. She likes to watch cartoons like the little girl she is.
                  We all wish she was be prettier but we work with whatever we have... sucks though.<br></li></ol>


                </span>
              </dd>


              <dt>
                <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading help3"  style="position: absolute;margin-top: 6px;margin-left: 0;height: 530px;;opacity: 0.01;z-index: 1;width: 353px;">
                  <!-- Heading -->
                    
                </span></a>
              </dt>
              <dd style="display: none;">
                <span class="acc_content" style="z-index:2">
                  <!-- Tab content -->
                  <h4>Privacy</h4>

                  Your privacy is very important to us. We designed our Data Use Policy to make important disclosures about how you can use InvertedCommas to share with others and how we collect and can use your content and information.<br>
                  <br>
                  <div class="span6"> 
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#help_tab_y1" data-toggle="tab">Sharing</a></li>
                      <li><a href="#help_tab_y2" data-toggle="tab">Safety</a></li>
                      <li><a href="#help_tab_y3" data-toggle="tab">Registration and Account Security</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane fade in active" id="help_tab_y1">
                        <p class="tiny_font"><b>You own all of the content and information you post on InvertedCommas, and you can control how it is shared through your privacy and application settings. In addition:</b><br><br>
                          For content that is covered by intellectual property rights, like photos and videos (IP content), you specifically give us the following permission, subject to your privacy and application settings: you grant us a non-exclusive, transferable, sub-licensable, royalty-free, worldwide license to use any IP content that you post on or in connection with InvertedCommas (IP License). This IP License ends when you delete your IP content or your account unless your content has been shared with others, and they have not deleted it.<br>
                          We always appreciate your feedback or other suggestions about InvertedCommas, but you understand that we may use them without any obligation to compensate you for them (just as you have no obligation to offer them).</p>
                        </div>
                        <div class="tab-pane fade" id="help_tab_y2">
                          <p><b>We do our best to keep InvertedCommas safe, but we cannot guarantee it. We need your help to keep InvertedCommas safe, which includes the following commitments by you:</b><br>
                            <ol><li>You will not post unauthorized commercial communications (such as spam) on InvertedCommas.</li>
                              <li>You will not collect users' content or information, or otherwise access InvertedCommas, using automated means (such as harvesting bots, robots, spiders, or scrapers) without our prior permission.</li>
                              <li>You will not solicit login information or access an account belonging to someone else.</li>
                              <li>You will not bully, intimidate, or harass any user.</li>
                              <li>You will not post content that: is hate speech, threatening, or pornographic; incites violence; or contains nudity or graphic or gratuitous violence.</li>
                              <li>You will not develop or operate a third-party application containing alcohol-related, dating or other mature content (including advertisements) without appropriate age-based restrictions.</li>
                              <li>You will not use InvertedCommas to do anything unlawful, misleading, malicious, or discriminatory.</li></ol></p> 
                            </div>

                            <div class="tab-pane fade" id="help_tab_y3">
                              <b>InvertedCommas users provide their real names and information, and we need your help to keep it that way. Here are some commitments you make to us relating to registering and maintaining the security of your account:</b><br>
                              <ol><li>You will not provide any false personal information on InvertedCommas, or create an account for anyone other than yourself without permission.</li>
                                <li>You will not create more than one personal account.</li>
                                <li>If we disable your account, you will not create another one without our permission.</li>
                                <li>You will not use your personal timeline primarily for your own commercial gain, and will use a InvertedCommas Page for such purposes.</li>
                                <li>You will not use InvertedCommas if you are a convicted sex offender.</li>
                                <li>You will not share your password (or in the case of developers, your secret key), let anyone else access your account, or do anything else that might jeopardize the security of your account.</li>
                                <li>You will not transfer your account (including any Page or application you administer) to anyone without first getting our written permission.</li>
                              </ol>
                            </div>

                          </div>
                          <br>
                        </div>





                      </span>
                    </dd>




                                <dt>
              <a href="" class="normal"><span class="closeOpen" style="background-position: 0px 0px;"></span><span class="acc_heading help4" id="helpQuadra" style="position: absolute;margin-top: 6px;
margin-left: 365px;height: 595px;opacity: 0.01;z-index: 1;width: 353px;">
                <!-- Heading -->
                 
              </span></a>
            </dt>
            <dd style="display: none;">
              <span class="acc_content" style="z-index:2">
 <h3> Submit a ticket</h3>
                <form name="submitTicket " method="get">
                      <div class="settingstag span3" > 
                        <span>Name: </span>
                        </div>
                         <input type="text" value="" id="ticketName" name="ticketName" class="settingsinput span3"  /><br><br>

<div class="settingstag span3"> 
                        <span>Email: </span>
                        </div>
                        <input type="text" value="{$email}" id="ticketEmail" name="ticketEmail" class="settingsinput span3"  />
<br><br>

<div class="settingstag span3"> 
                        <span>Subject: </span>
                        </div>
                        <input type="text" value="" id="ticketSubject" name="ticketSubject" class="span3 settingsinput"  />
<br><br>
<div class="settingstag span3"> 
                        <span>Message: </span>
                        </div><textarea class="span3 settingsinput" id="ticketMessage" name="ticketMessage" rows="4" cols="50"></textarea>
<br><br>
<div class="settingstag span3" style="opacity:0.01"> </div><button type="submit" id="ticketSend"  class="button transparent span3 alignLeft settingssubmit" style="background-color:#5cb85c!important;margin-left: 0;">Send us a ticket!</button>

                      </form>

                </span>
              </dd>

                  </dl>


                  <div class="row-fluid">                          
                    <div class="span6"> 
                      <a class="services right"> 
                        <div class="description">                
                          <h5 class="upperCase">Terms and Conditions </h5>
                          <p style="height: 80px;">Super smart stuff. Laws are the real deal. Check your privilege homie.</p>
                        </div>
                        <span class="service_icon"><img src="{$BASE_URL}images/help/terms.png" alt="image01"></span>
                      </a>
                      <hr class="separator">
                    </div>

                    <div class="span6"> 
                      <a class="services" > 
                        <span class="service_icon"><img src="{$BASE_URL}images/help/ticket.png" alt="image02"></span>
                        <div class="description">                
                          <h5 class="upperCase">Submit a help ticket </h5>
                          <p>Here at InvertedCommas our team is ready to help you with your needs and hear your suggestions. Just submit a ticket and we'll get in contact with you!
                          </div>
                        </a>
                      </div>                            
                    </div>

                    <br><br><br><br><br><br>

                  </div>    
                </div>
              </div>
