<div data-id="!searchresults" class="contentWrapper darkStyle" >

  <div class="m-Scrollbar"> 

    <hr class="alignToMiddle" />

    <div class="container-fluid">       
      <div class="row-fluid">
        <div class="span12"> 
          <div class="heading_stroke"> 
            <div class="heading_stroke_wrapper">
              <div class="stroke-text"><h2>Search Results</h2></div>
              <div class="stroke-holder"><div class="stroke-line"></div></div>
              </div>
            </div>
          </div>
        </div>

        
      <div id="totalpage">
        <div id="totalpage2"><h3>How to use our search engine:</h3>
         <div class="row-fluid" style="margin-bottom: 12px;">
          <div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">
            <h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" >Searching for Users:</h7>
            <h6 style="padding-left: 34px;">
              user (string)
            </h6>
            <hr>
          </div>
        </div>

        <div class="row-fluid" style="margin-bottom: 12px;">
          <div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">
            <h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" > Searching for Quotes:</h7>
            <h6 style="padding-left: 34px;">
              quote (string)
            </h6>
            <hr>
          </div>
        </div>

        <div class="row-fluid" style="margin-bottom: 12px;">
          <div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">
            <h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" > Searching for Quotes from a specific Author:</h7>
            <h6 style="padding-left: 34px;">
              quote (string) from (string)
            </h6>
            <hr>
          </div>
        </div>

        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div> 
      </div>

    </div>
  </div>
</div>