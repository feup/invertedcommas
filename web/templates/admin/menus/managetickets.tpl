{include file='admin/common/header.tpl'}

<div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{$BASE_URL}"><img src="{$BASE_URL}images/admin/logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="manageusers.php"><i class="fa fa-bar-chart-o"></i> Manage Users</a></li>
            <li class="active"><a href="#"><i class="fa fa-ticket"></i> Manage Tickets</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Website Settings <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">General Settings</a></li>
                <li><a href="#">Maintenance Mode</a></li>
                <li><a href="#">Spam Control</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {$USERNAME|escape} <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{$BASE_URL}/pages/home/home.php#!profile"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="{$BASE_URL}"><i class="fa fa-globe"></i> Visit Website </a></li>
                <li class="divider"></li>
                <li><a href="{$BASE_URL}/actions/users/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

  <div id="page-wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h1>Manage Tickets </h1>
      </div>
    </div><!-- /.row -->

    <div class="row">
      <div class="col-lg-12">

        <h2>Tickets</h2>
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped tablesorter paginated">
            <thead>
              <tr>
                <th class="header">User ID <i class="fa fa-sort"></i></th>
                <th class="header">Username <i class="fa fa-sort"></i></th>
                <th class="header">Email <i class="fa fa-sort"></i></th>
                <th class="header">Estado <i class="fa fa-sort"></i></th>
              </tr>
            </thead>
            <tbody id="tickets">
              
            </tbody>
          </table>
        </div>
      </div>
    </div><!-- /.row -->
  </div>
</div><!-- /.row -->





{include file='admin/common/footer.tpl'}