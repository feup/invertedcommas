<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>InvertedCommas - Admin Control Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="{$BASE_URL}css/admin/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="{$BASE_URL}css/admin/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="{$BASE_URL}font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{$BASE_URL}css/admin/manageusers.css">
    <script src="{$BASE_URL}javascript/admin/jquery-1.10.2.js"></script>
<script src="{$BASE_URL}javascript/admin/trello.js"></script> 
<script src="{$BASE_URL}javascript/admin/manageusers.js"></script> 
<script src="{$BASE_URL}javascript/admin/hometickets.js"></script> 
<script src="{$BASE_URL}javascript/admin/managetickets.js"></script> 
<script src="https://api.trello.com/1/client.js?key=00f1dd1e00000f1dd1e00000f1dd1e00&dummy=.js"></script>
    
    <!-- Page Specific CSS -->

    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
  </head>

  <body>


   