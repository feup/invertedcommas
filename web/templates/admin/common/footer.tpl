    <!-- JavaScript -->
    
    <script src="{$BASE_URL}javascript/admin/bootstrap.js"></script>

    <!-- Page Specific Plugins -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="{$BASE_URL}javascript/admin/morris/chart-data-morris.js"></script>
    <script src="{$BASE_URL}javascript/admin/tablesorter/jquery.tablesorter.js"></script>
    <script src="{$BASE_URL}javascript/admin/tablesorter/tables.js"></script>

  </body>
</html>
