<?php

	function addTicket($id, $contacto,$descricao, $estado)
	{
		global $conn;
		$stmt = $conn->prepare("INSERT INTO pedidosuporte (iduser,contacto,descricao,estado) VALUES (?, ?, ?, ?)");
		$stmt->execute(array($id, $contacto, $descricao, $estado));
		return true;
	}

	function getUnsolvedTickets()
	{
		global $conn;
		$stmt = $conn->prepare("SELECT count(*) from pedidosuporte where estado='em espera'");
		$stmt->execute();
		$res = $stmt->fetch();
		return $res['count'];
	}

	function getTotalTickets()
	{
		global $conn;
		$stmt = $conn->prepare("SELECT count(*) from pedidosuporte");
		$stmt->execute();
		$res = $stmt->fetch();
		return $res['count'];
	}

	function getSolvedTickets()
	{
		global $conn;
		$stmt = $conn->prepare("SELECT count(*) from pedidosuporte where estado='resolvido'");
		$stmt->execute();
		$res = $stmt->fetch();
		return $res['count'];
	}

	function getTickets()
	{
		global $conn;
		$stmt = $conn->prepare("SELECT iduser,nome,contacto,descricao,estado from pedidosuporte, utilizador where iduser=utilizador.id");
		$stmt->execute();
		$res = $stmt->fetchAll();
		return $res;
	}



?>

