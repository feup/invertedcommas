<?php
	function getQuotesFeed($idUser){
		global $conn;
		$stmt = $conn->prepare("SELECT citacao.id AS idcitacao, citacao.data , citacao.textoquote , citacao.fonte , friends.nome , friends.id, utilizador.email , utilizador.id FROM (citacao
	      								LEFT JOIN utilizador AS quoter  ON ( 
	                        				citacao.iduser IN (SELECT amizade.iduser1 FROM amizade WHERE amizade.iduser2 = quoter.id ) 
	                        				OR
	                        				citacao.iduser IN (SELECT amizade.iduser2 FROM amizade WHERE amizade.iduser1 = quoter.id )
	      								)
	      								LEFT JOIN utilizador AS friends ON ( 
	                        				citacao.iduser = friends.id
	      								)
										INNER JOIN utilizador ON citacao.iduser=utilizador.id
	    							)
    								WHERE quoter.id = :id
									UNION
								SELECT citacao.id AS idcitacao, citacao.data , citacao.textoquote , citacao.fonte , seguido.nome , seguido.id, utilizador.email, utilizador.id FROM citacao
    								LEFT JOIN utilizador AS quoter  ON ( 
                      					citacao.iduser IN (SELECT seguidor.idseguido FROM seguidor WHERE seguidor.idseguidor = quoter.id ) 
    								)
    								LEFT JOIN utilizador AS seguido ON ( 
                     					 citacao.iduser = seguido.id
    								)
									INNER JOIN utilizador ON citacao.iduser=utilizador.id
   									WHERE quoter.id = :id
    							ORDER BY 1 DESC
    							LIMIT 40"
		);


		$stmt->bindParam(":id",$idUser);
		$stmt->execute();


	 	$finalArray = array();

	    while ($row = $stmt->fetch() ) {

			$stmt1 = $conn->prepare("SELECT COUNT(*) AS contador FROM upvote, citacao WHERE
									upvote.idcitacao = citacao.id AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$resultA = $stmt1->fetch();

			$stmt1 = $conn->prepare("SELECT * FROM upvote, utilizador, citacao WHERE
									upvote.idcitacao = citacao.id AND
									upvote.iduser = :userParam AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":userParam", $idUser);
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$mayUpvote = !($stmt1->fetch());


	        $finalArray[] = array(
	            'User'  	=> $row['nome'],
	            'IDCitacao' => $row['idcitacao'],
	            'Texto' 	=> $row['textoquote'],
	            'Fonte' 	=> $row['fonte'],
	            'Upvotes' 	=> $resultA['contador'],
	            'PodeUp'	=> $mayUpvote,
	            'Data' 		=> $row['data'],
	            'userID'    => $row['id'],
	            'Email'		=> $row ['email']);
	    }

	    //var_dump($finalArray);
	    return $finalArray;



	}
 	 	

?>