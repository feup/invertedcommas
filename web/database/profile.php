<?php


	function addFriendRequest($idEmissor, $idRecetor){
		global $conn;

		$stmt = 'INSERT INTO pedidoamizade(data,estado,idemissor,idrecetor) VALUES(now(), :estado, :iduser1, :iduser2)';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':estado', $a="em espera",PDO::PARAM_STR);
		$stmt->bindParam(':iduser1', $idEmissor,PDO::PARAM_INT);
		$stmt->bindParam(':iduser2', $idRecetor,PDO::PARAM_INT);
		return $stmt->execute();
	}

	function addFollower($idSeguidor, $idSeguido){
		global $conn;

		$stmt = 'INSERT INTO seguidor(data,idseguidor,idseguido) VALUES(now(), :iduser1, :iduser2)';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser1', $idSeguidor);
		$stmt->bindParam(':iduser2', $idSeguido);
		return $stmt->execute();
	}

	function addFriend($id1, $id2){
		global $conn;

		$stmt = 'INSERT INTO amizade(iduser1,iduser2) VALUES(:iduser1, :iduser2)';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser1', $id1);
		$stmt->bindParam(':iduser2', $id2);
		return $stmt->execute();
	}


	function unfollow($id1, $id2){
		global $conn;

		$stmt = 'DELETE FROM seguidor WHERE idseguidor = :id1 AND idseguido = :id2';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id1', $id1);
		$stmt->bindParam(':id2', $id2);
		return $stmt->execute();
	}

	function removeFriend($id1, $id2){
		global $conn;

		$stmt = 'DELETE FROM amizade WHERE iduser1 = :id1 AND iduser2 = :id2';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id1', $id1);
		$stmt->bindParam(':id2', $id2);
		$stmt->execute();

		$stmt = 'DELETE FROM amizade WHERE iduser1 = :id1 AND iduser2 = :id2';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id1', $id2);
		$stmt->bindParam(':id2', $id1);
		$stmt->execute();

		return $stmt->execute();
	}

	function removeFriendRequest($id1, $id2){
		global $conn;

		$stmt = 'DELETE FROM pedidoamizade WHERE idemissor = :id1 AND idrecetor = :id2';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id1', $id1);
		$stmt->bindParam(':id2', $id2);
		return $stmt->execute();
	
	}


	function getFriends($idUser){
		global $conn;

		$stmt = 'SELECT amizade.iduser2 AS id, utilizador.nome AS nome, utilizador.email AS email FROM amizade, utilizador 
					WHERE amizade.iduser1 = :iduser 
					AND utilizador.id = amizade.iduser2';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->execute();

		$array = array();
	    while ($row = $stmt->fetch()) {
	        $array[] = array(
	            'ID' => $row['id'],
	            'Nome' => $row['nome'],
	            'Email' => $row['email']);
	    }

	    $stmt = 'SELECT amizade.iduser1 AS id, utilizador.nome AS nome, utilizador.email AS email FROM amizade, utilizador 
	    			WHERE amizade.iduser2 = :iduser 
	    			AND utilizador.id = amizade.iduser1';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->execute();

	    while ($row = $stmt->fetch()) {
	        $array[] = array(
	            'ID' => $row['id'],
	            'Nome' => $row['nome'],
	            'Email' => $row['email']);
	    }

   		return $array;
	}

	function getFollowers($idUser){
		global $conn;

		$stmt = 'SELECT seguidor.idseguidor AS id, utilizador.nome AS nome, utilizador.email AS email FROM seguidor, utilizador 
					WHERE seguidor.idseguidor = utilizador.id 
					AND seguidor.idseguido = :iduser';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->execute();

		$array = array();
	    while ($row = $stmt->fetch()) {
	        $array[] = array(
	            'ID' => $row['id'],
	            'Nome' => $row['nome'],
	            'Email' => $row['email']);
	    }

   		return $array;
	}

	function getFollowing($idUser){
		global $conn;

		$stmt = 'SELECT seguidor.idseguido AS id, utilizador.nome AS nome, utilizador.email AS email FROM seguidor, utilizador 
				WHERE seguidor.idseguidor = :iduser 
				AND seguidor.idseguido = utilizador.id';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->execute();

		$array = array();
	    while ($row = $stmt->fetch()) {
	        $array[] = array(
	            'ID' => $row['id'],
	            'Nome' => $row['nome'],
	            'Email' => $row['email']);
	    }

   		return $array;
	}

	function updateBio($idUser, $newBio){
		global $conn;

		$stmt = 'UPDATE utilizador SET bio = :newBio WHERE id = :id';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':newBio', $newBio);
		$stmt->bindParam(':id', $idUser);
		return $stmt->execute();

	}

	function changePermissions($idUser, $newPerfilPublico, $newProcuravel){
		global $conn;

		$stmt = 'UPDATE utilizador SET perfilpublico = :perfilpublico, procuravel = :procuravel WHERE id = :id';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':perfilpublico', $newPerfilPublico);
		$stmt->bindParam(':procuravel', $newProcuravel);
		$stmt->bindParam(':id', $idUser);
		return $stmt->execute();

	}


	function upvoteQuote($idUser, $idCitacao){
		global $conn;

		$stmt = 'INSERT INTO upvote(data,iduser,idcitacao) VALUES(now(), :iduser, :idcitacao)';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->bindParam(':idcitacao', $idCitacao);
		return $stmt->execute();
	}

	function deleteUpvote($idUser, $idCitacao){
		global $conn;
		$stmt = 'SELECT id FROM upvote WHERE iduser = :iduser AND idcitacao = :idcitacao';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->bindParam(':idcitacao', $idCitacao);
		$stmt->execute();

		$res = $stmt->fetch();
		$idUpvote = $res['id'];

		if($idUpvote == null){
			$stmt = 'DELETE FROM upvote WHERE iduser = :iduser AND idcitacao = :idcitacao';
			$stmt = $conn->prepare($stmt);
			$stmt->bindParam(':iduser', $idUser);
			$stmt->bindParam(':idcitacao', $idCitacao);
			return $stmt->execute();
		}
		else{
			$stmt = 'DELETE FROM notificacao WHERE upvote = :idUpvote';
			$stmt = $conn->prepare($stmt);
			$stmt->bindParam(':idUpvote', $idUpvote);
			$stmt->execute();

			$stmt = 'DELETE FROM upvote WHERE id = :id';
			$stmt = $conn->prepare($stmt);
			$stmt->bindParam(':id', $idUpvote);
			return $stmt->execute();
		}
	}

	function deleteQuote($idCitacao){
		
		global $conn;
		$stmt = 'DELETE FROM citacao WHERE id = :idcitacao';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':idcitacao', $idCitacao);
		$stmt->execute();

	}

	

	function mayUpvote($idUser, $idCitacao){
		global $conn;

		$stmt = 'SELECT * FROM upvote WHERE upvote.idcitacao = :idcitacao AND upvote.iduser = :iduser';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':idcitacao', $idCitacao);
		$stmt->bindParam(':iduser', $idUser);
		$stmt->execute();

		if($stmt->fetch()) return false;
		else return true;
	}




?>