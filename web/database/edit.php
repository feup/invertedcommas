<?php
	
	function editName($id,$newName,$password){
		global $conn;
		$pw_hash =  hash('sha256', $password);
		$stmt1 = "SELECT * FROM utilizador, utilizadoremail 
					WHERE utilizadoremail.passhash = :pass
					AND utilizador.id = utilizadoremail.iduser
					AND utilizador.id = :id";
		$stmt1 = $conn->prepare($stmt1);
		$stmt1->bindParam(':pass', $pw_hash);
		$stmt1->bindParam(':id', $id);
		$stmt1->execute();
		if(!($stmt1->fetch())) return false;

		echo "Credenciais correctas";

		$stmt2 = "UPDATE utilizador SET nome = :newName WHERE id = :id";
		$stmt2 = $conn->prepare($stmt2);
		$stmt2->bindParam(':newName', $newName);
		$stmt2->bindParam(':id', $id);

		$result = $stmt2->execute();
		$result = $stmt2->fetch();
		return ( $result === false ? false  : true );
	}

	function editPassword($id,$oldPassword,$newPassword){
		global $conn;
		$oldHash =  hash('sha256', $oldPassword);
		$newHash =  hash('sha256', $newPassword);

		$stmt = "SELECT * FROM utilizador, utilizadoremail 
					WHERE utilizadoremail.passhash = :pass
					AND utilizador.id = utilizadoremail.iduser
					AND utilizador.id = :id";
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':pass', $oldHash);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		if(!($stmt->fetch())) return false;

		$stmt = "UPDATE utilizadoremail SET passhash = :newPass WHERE utilizadoremail.iduser = :id AND passHash = :oldPass";
		$stmt = $conn->prepare($stmt);

		$stmt->bindParam(':newPass', $newHash);
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':oldPass', $oldHash);

		$stmt->execute();
		$result = $stmt->fetch();
		
		$result = ( $result === false ? false  : true );
		return ( $result === false ? false  : true );
		
	}
	function editEmail($id,$newEmail){
		global $conn;
		
		$stmt = "UPDATE utilizadoremail SET email = :newEmail WHERE iduser = :id";
		$stmt = $conn->prepare($stmt);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':newEmail', $newEmail);

		$stmt->execute();

		$result = $stmt->fetch();

		$result = ( $result === false ? false  : true );

		return $result;

	}
	function editPrivacy($id,$profile,$friends,$followers){
		global $conn;
		$stmt = "UPDATE utilizador SET privperfil = :privPerf ,
									   privamigos = :privamigos ,
									   privseguidores = :privseguidores
									WHERE id = :id "; 
		
		$stmt = $conn->prepare($stmt);

		$stmt->bindParam(':id'				, $id);
		$stmt->bindParam(':privPerf'		, $profile);
		$stmt->bindParam(':privamigos'		, $friends);
		$stmt->bindParam(':privseguidores'	, $followers);

		$stmt->execute();

		$result = $stmt->fetch();
		echo($result);
		//$result = ( $result === false ? false  : true );

	}

?>