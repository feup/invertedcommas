<?php
  

  function insertQuote($username, $quote, $fonte) {
    global $conn;

    $stmt = $conn->prepare("INSERT INTO citacao (iduser,data,textoquote,fonte)
                            VALUES (?, 'now()', ?, ?)");
    $stmt->execute(array($username, $quote, $fonte));
    
  }


  function getQuotesByUser($idUser){
    global $conn;

    $stmt = $conn->prepare("SELECT utilizador.id,utilizador.nome,citacao.data, citacao.id AS idcitacao, citacao.textoquote, citacao.fonte, utilizador.email
              FROM utilizador LEFT OUTER JOIN citacao ON(citacao.iduser = utilizador.id)
              WHERE  utilizador.id = :id ORDER BY citacao.data DESC");
 
    $stmt->bindParam(":id",$idUser);
    $stmt->execute();

    $finalArray = array();

    while ($row = $stmt->fetch() ) {

        $stmt1 = $conn->prepare("SELECT COUNT(*) AS contador FROM upvote, citacao WHERE
                    upvote.idcitacao = citacao.id AND
                    citacao.id = :idCitacao");
        $stmt1->bindParam(":idCitacao", $row['idcitacao']);
        $stmt1->execute();
        $resultA = $stmt1->fetch();

        $stmt1 = $conn->prepare("SELECT * FROM upvote, utilizador, citacao WHERE
                    upvote.idcitacao = citacao.id AND
                    upvote.iduser = :userParam AND
                    citacao.id = :idCitacao");
        $stmt1->bindParam(":userParam", $_SESSION['id']);
        $stmt1->bindParam(":idCitacao", $row['idcitacao']);
        $stmt1->execute();
        $mayUpvote = !($stmt1->fetch());
      
        $finalArray[] = array(
            'User'  => $row['nome'],
            'IDCitacao' => $row['idcitacao'],
            'Texto' => $row['textoquote'],
            'Fonte' => $row['fonte'],
            'Data' => $row['data'],
            'Upvotes' => $resultA['contador'],
            'PodeUp' => $mayUpvote,
            'Email' => $row['email']);
    }
    return $finalArray;
  }

  function getQuotesBySource($fonte){
    global $conn;

    $stmt = $conn->prepare("SELECT citacao.iduser citacao.data, citacao.textoquote, utilizador.email
              FROM citacao INNER JOIN utilizadoremail ON utilizador.id=utilizadoremail.iduser
              WHERE citacao.fonte = ?");

    $stmt->execute(array($fonte));

    $finalArray = array();
    while ($row = $stmt->fetch()) {
        $finalArray[] = array(
            'IDUtilizador' => $row['iduser'],
            'Texto' => $row['textoquote'],
            'Data' => $row['data'],
            'Email' => $row['email']);
    }

    return $finalArray;
  }

    function getQuoteByID($id){
      global $conn;
      $stmt = $conn->prepare("SELECT id,textoquote,fonte,iduser,data 
                              FROM citacao 
                              WHERE id = :id2");
      $stmt->bindParam(":id2",$id);
      $stmt->execute();
      $res = array();
      $res = $stmt->fetch();
      return $res;
    }

    function getQuoteUpvotes($id){
        global $conn;
        $stmt1 = $conn->prepare("SELECT COUNT(*) AS contador FROM upvote, citacao WHERE upvote.idcitacao = citacao.id AND citacao.id = :idCitacao");
        $stmt1->bindParam(":idCitacao", $id);
        $stmt1->execute();
        $resultA = $stmt1->fetch();
        return $resultA['contador'];
    }

  /*
  
  function getTweetCountAfter($id) {
    global $conn;
    $stmt = $conn->prepare("SELECT COUNT(*) AS count
                            FROM tweets 
                            WHERE id > ?");
    $stmt->execute(array($id));
    $result = $stmt->fetch();    
    return $result['count'];
  }

  function getTweetsAfter($id) {
    global $conn;
    $stmt = $conn->prepare("SELECT * 
                            FROM tweets JOIN 
                                 users USING(username) 
                            WHERE id > ?
                            ORDER BY time");
    $stmt->execute(array($id));
    return $stmt->fetchAll();
  }


    function getAllTweets() {
    global $conn;
    $stmt = $conn->prepare("SELECT * 
                            FROM tweets JOIN 
                                 users USING(username) 
                            ORDER BY time DESC");
    $stmt->execute();
    return $stmt->fetchAll();
  }
  
  function getUserTweets($username) {
    global $conn;
    $stmt = $conn->prepare("SELECT * 
                            FROM tweets JOIN 
                                 users USING(username) 
                            WHERE username = ? 
                            ORDER BY time DESC");
    $stmt->execute(array($username));
    return $stmt->fetchAll();
  }*/
?>
