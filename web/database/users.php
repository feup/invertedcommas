<?php
  
	//include_once($BASE_DIR .'lib/password.php'); 	

	  function createUser($username, $password, $confpass, $email, $datanascimento) {

	    global $conn;
	    
	    $validNewUser = true;

	    // Confirma as passwords
	    if(!($password == $confpass)){
	    	$validNewUser = false;
			$message = "Passwords don't match";
			$code = 101;
			throw new PDOException($message, $code );
	    }

	    // Verifica se Username esta em uso
		$stmt = $conn->prepare("SELECT * FROM utilizador WHERE utilizador.nome = ?");
		$stmt->execute(array($username));
		if($stmt->fetch() == true){ // Se encontrou resultados, nao pode inserir
			$validNewUser = false;
			$message = 'Username is already in use';
			$code = 101;
			throw new PDOException($message, $code );
		}

		// Verifica se Email esta em uso
		$stmt = $conn->prepare("SELECT * FROM utilizador WHERE utilizador.email = ?");
		$stmt->execute(array($email));
		if($stmt->fetch() == true){ // Se encontrou resultados, nao pode inserir
			$validNewUser = false;
			$message = 'E-mail is already in use';
			$code = 101;
			throw new PDOException($message, $code );
		}


		if($validNewUser){

			$stmt = $conn->prepare("INSERT INTO utilizador (datanascimento,nome,email) VALUES (?, ?, ?)");
		    $hash = hash('sha256', $password);
		    $stmt->execute(array($datanascimento, $username, $email));

		    $stmt = $conn->prepare("SELECT MAX(id) FROM utilizador");
		    $stmt->execute();
		    $result = $stmt->fetch();
		    $lastid = $result['max'];

		    
		    $stmt = $conn->prepare("INSERT INTO utilizadoremail (iduser,passhash) VALUES (?, ?)");
		    $stmt->execute(array($lastid,$hash));

		}
		else{
			return false;
		}

	  }


	  function isLoginCorrect($username, $password) {
	    global $conn;
	    $stmt = $conn->prepare("SELECT * 
	                            FROM utilizador, utilizadoremail
	                            WHERE utilizador.id = utilizadoremail.iduser 
	                            AND utilizador.nome = ? AND utilizadoremail.passhash = ?");
	    $hash = hash('sha256', $password);
	    $stmt->execute(array($username,$hash));
	  	return $stmt->fetch() == true;
	  }


	  function LoginFacebook($data) {

	    global $conn;
	
	    $stmt = $conn->prepare("SELECT * 
	                            FROM utilizadorfacebook
	                            WHERE idfacebook = ?");
	    $stmt->execute(array($data['id']));
	  	if($stmt->fetch())
	  	{
	  		$arr = array("username"=> $data['name']);

	  		$stmt = $conn->prepare("SELECT utilizador.id AS id FROM utilizador, utilizadorfacebook 
	  			WHERE utilizador.id = utilizadorfacebook.iduser
	  			AND utilizadorfacebook.idfacebook = ?");
		    $stmt->execute(array($data['id']));
		    $result = $stmt->fetch();
		    $lastid = $result['id'];

		    $arr['id'] = $lastid;
		    $arr['success'] = true;

	  		return $arr;
	  	}
	  	else
	  	{
	  		$stmt = $conn->prepare("INSERT INTO utilizador (datanascimento,nome,email) VALUES (?, ?, ?)");
	  		$stmt->execute(array($data['birthday'], $data['name'], $data['email']));

	  		$stmt = $conn->prepare("SELECT MAX(id) FROM utilizador");
		    $stmt->execute();
		    $result = $stmt->fetch();
		    $lastid = $result['max'];

	  		$stmt = $conn->prepare("INSERT INTO utilizadorfacebook (iduser,idfacebook) VALUES (?, ?)");
	  		$stmt->execute(array($lastid, $data['id']));

	  		$arr = array("username"=> $data['name'], "id"=> $lastid, "success" => true);
	  		return $arr;
	  	
	  	}
	  	$arr = array("success" => false);
	  	return $arr;

	  }


	  function getIdByUser($username){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT id 
	                            FROM utilizador 
	                            WHERE nome = ?");
	    $stmt->execute(array($username));
	    $res = array();
	    $res = $stmt->fetch();
	  	return $res['id'];
	  }

	  function getUserByID($id){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT id,nome,email 
	                            FROM utilizador 
	                            WHERE id = :id2");
	    $stmt->bindParam(":id2",$id);
	    $stmt->execute();
	    $res = array();
	    $res = $stmt->fetch();
	  	return $res;
	  }

	  
	  function getEmailByID($id){
	  	global $conn;
	  	$stmt = $conn->prepare("SELECT email 
	  		    				FROM utilizador
	                            WHERE id = ?");
	    $stmt->execute(array($id));
	    $res = array();
	    $res = $stmt->fetch();
	  	return $res['email'];
	  }
	  

	  function validateEmail($email){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT utilizador.id FROM utilizador WHERE utilizador.email = ?;");
	    $stmt->execute(array($email));
	    $res = array();
	    return $stmt->fetch();
	  }

	  	function listUsers() {
			  	global $conn;
	  		    $stmt = $conn->prepare("SELECT * FROM utilizador ORDER BY utilizador.id");

	    		$stmt->execute();
	    		$finalArray = array();
    			while ($row = $stmt->fetch()) {

	  		    $stmt2 = $conn->prepare("SELECT COUNT(*) AS contador FROM utilizador, citacao WHERE
                    citacao.iduser = :id AND utilizador.id = citacao.iduser");
				$stmt2->bindParam(":id",$row['id']);
	    		$stmt2->execute();
	    		$resultplz = $stmt2->fetch();


        			$finalArray[] = array(
            			'ID' => $row['id'],
            			'Nome' => $row['nome'],
            			'TotalQuotes' => $resultplz['contador']);
   				}

	  			return $finalArray;
	  }

	  function totalQuotesByID($idUser) {
			  	global $conn;
	  		    $stmt2 = $conn->prepare("SELECT COUNT(*) AS contador FROM utilizador, citacao WHERE
                    citacao.iduser = :id AND citacao.iduser = utilizador.id");
				$stmt2->bindParam(":id",$row['iduser']);
	    		$stmt2->execute();
	    		$resultplz = $stmt2->fetch();

	  			return $resultplz['contador'];
	  }
?>
