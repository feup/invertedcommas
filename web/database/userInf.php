<?php

	function getBio($id){

		global $conn;

		$stmt = "SELECT bio FROM utilizador  WHERE utilizador.id = :id";
		$stmt = $conn->prepare($stmt);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$result = $stmt->fetch();

		return ( $result['bio'] );
		
	}

	function getNumberFriends($id){
		global $conn;
	    $stmt = $conn->prepare("SELECT count(utilizador.id)
								  FROM utilizador LEFT OUTER JOIN amizade ON(utilizador.id = amizade.iduser1 OR utilizador.id = amizade.iduser2)
								  WHERE amizade.iduser1= ? OR amizade.iduser2 = ?");
	    $stmt->execute(array($id,$id));
	    $res = array();
	    $res = $stmt->fetch();


	  	return $res['count']/2;
	  }

	function getNumberFollowers($id){
		global $conn;
	    $stmt = $conn->prepare("SELECT count(utilizador.id)
								  FROM utilizador LEFT OUTER JOIN seguidor ON(utilizador.id = seguidor.idseguido)
								  WHERE seguidor.idseguido = ?;");
	    $stmt->execute(array($id));
	    $res = array();
	    $res = $stmt->fetch();

	  	return $res['count'];
	  }

	 function getNumberFollowing($id){
		global $conn;
	    $stmt = $conn->prepare("SELECT count(utilizador.id)
								  FROM utilizador LEFT OUTER JOIN seguidor ON(utilizador.id = seguidor.idseguidor)
								  WHERE seguidor.idseguidor = ?;");
	    $stmt->execute(array($id));
	    $res = array();
	    $res = $stmt->fetch();

	  	return $res['count'];
	  }

	  function isFollowing($id1, $id2){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT id FROM seguidor WHERE 
	    			idseguidor = ? AND idseguido= ?");
	    $stmt->execute(array($id1,$id2));
	    $res = array();
	    if($stmt->fetch()) return true;
	    else return false;
	  }


	  function isFriend($id1, $id2){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT id FROM amizade WHERE 
	    			iduser1 = ? AND iduser2 = ?");
	    $stmt->execute(array($id1,$id2));

		$stmt3 = $conn->prepare("SELECT id FROM pedidoamizade WHERE 
	    			idemissor = ? AND idrecetor = ? AND estado = 'em espera'");
	    $stmt3->execute(array($id1,$id2));
	    if($stmt3->fetch()) return "pending";
	    else if ($stmt->fetch() == NULL){
	    	$stmt2 = $conn->prepare("SELECT id FROM amizade WHERE 
	    			iduser1 = ? AND iduser2 = ?");
		    $stmt2->execute(array($id2,$id1));

		    if($stmt2->fetch()) return "true";
		    else return "false";
	    }
	    else{
	    	return "true";
	    }
	    
	  }

	  function getPermissions($id){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT privperfil,privamigos,privseguidores FROM utilizador WHERE 
	    			id = ?");
	    $stmt->execute(array($id));
	    return $stmt->fetch();
	  }
	  function getName($id){
	  	global $conn;
	    $stmt = $conn->prepare("SELECT nome FROM utilizador WHERE 
	    			id = ?");
	    $stmt->execute(array($id));
	    return $stmt->fetch();
	  }

	

?>