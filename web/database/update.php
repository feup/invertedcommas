<?php
	
	function updatePostsProfile($idUser,$date){

		global $conn;

	    $stmt = $conn->prepare("SELECT utilizador.id,utilizador.nome,citacao.data, citacao.textoquote, citacao.fonte
	              FROM utilizador LEFT OUTER JOIN citacao ON(citacao.iduser = utilizador.id) 
	              WHERE utilizador.id = :id  AND citacao.data > :date ORDER BY citacao.data DESC");
	 
	    $stmt->bindParam(":id",$idUser);
	    $stmt->bindParam(":date",$date);
	    $stmt->execute();

		$finalArray = array();

	    while ($row = $stmt->fetch() ) {
	      
	      		 $finalArray[] = array(
	            'User'  => $row['nome'],
	            'Texto' => $row['textoquote'],
	            'Fonte' => $row['fonte'],
	            'Data' => $row['data']);
	      	
	    }
	    return $finalArray;
		
	}

	function updatePostsFeed($idUser, $date){
		global $conn;
		$stmt = $conn->prepare("SELECT citacao.data , citacao.textoquote , citacao.fonte , friends.nome , friends.id FROM (citacao
	      								LEFT JOIN utilizador AS quoter  ON ( 
	                        				citacao.iduser IN (SELECT amizade.iduser1 FROM amizade WHERE amizade.iduser2 = quoter.id ) 
	                        				OR
	                        				citacao.iduser IN (SELECT amizade.iduser2 FROM amizade WHERE amizade.iduser1 = quoter.id )
	      								)
	      								LEFT JOIN utilizador AS friends ON ( 
	                        				citacao.iduser = friends.id
	      								)
	    							)
    								WHERE quoter.id = :id AND citacao.data > :date
									UNION
								SELECT citacao.data , citacao.textoquote , citacao.fonte , seguido.nome , seguido.id FROM citacao
    								LEFT JOIN utilizador AS quoter  ON ( 
                      					citacao.iduser IN (SELECT seguidor.idseguido FROM seguidor WHERE seguidor.idseguidor = quoter.id ) 
    								)
    								LEFT JOIN utilizador AS seguido ON ( 
                     					 citacao.iduser = seguido.id
    								)
   									WHERE quoter.id = :id AND citacao.data > :date
    							ORDER BY 1 DESC
    							LIMIT 40"
		);


		$stmt->bindParam(":id",$idUser);
		$stmt->bindParam(":date",$date);
		$stmt->execute();
		

	 	$finalArray = array();

	    while ($row = $stmt->fetch() ) {
	      
		        $finalArray[] = array(
		            'User'  => $row['nome'],
		            'Texto' => $row['textoquote'],
		            'Fonte' => $row['fonte'],
		            'Data' => $row['data']);
	    	
	    }
	    return $finalArray;

	}
?>