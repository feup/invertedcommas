<?php

	function getFriendRequests($idUser){
		global $conn;

		$stmt = "SELECT pedidoamizade.data AS data, pedidoamizade.idemissor AS idemissor, pedidoamizade.estado AS estado, notificacao.id AS idnot
				FROM notificacao, pedidoamizade, utilizador 
				WHERE pedidoamizade.idrecetor = :id
				AND utilizador.id = :id
				AND notificacao.idpedamizade = pedidoamizade.id";
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id', $idUser);
		$stmt->execute();

		$finalArray = array();

	    while ($row = $stmt->fetch() ) {
	    	$idEmissor = $row['idemissor'];
	    	$stmt1 = "SELECT nome, email FROM utilizador 
				WHERE utilizador.id = :idEmissor";
			$stmt1 = $conn->prepare($stmt1);
			$stmt1->bindParam(':idEmissor', $idEmissor);
			$stmt1->execute();
			$result = $stmt1->fetch();

	        $finalArray[] = array(
	        	'IDNotificacao' => $row['idnot'],
	            'Data'  => $row['data'],
	            'IDEnvio' => $row['idemissor'],
	            'Email' => $result['email'],
	            'NomeEmissor' => $result['nome'],
	            'Estado' => $row['estado'],);
	    }
	    return $finalArray;
	   }

	function getNewFollowers($idUser){
		global $conn;

		$stmt = "SELECT notificacao.id AS idnot, seguidor.data AS data, seguidor.idseguidor AS idseguidor FROM seguidor, notificacao
				WHERE seguidor.idseguido = :id
				AND notificacao.idseguidor = seguidor.id";
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id', $idUser);
		$stmt->execute();

		$finalArray = array();

	    while ($row = $stmt->fetch() ) {
	    	

	    	$idEmissor = $row['idseguidor'];
	    	$stmt1 = "SELECT nome, email FROM utilizador 
				WHERE utilizador.id = :idEmissor";
			$stmt1 = $conn->prepare($stmt1);
			$stmt1->bindParam(':idEmissor', $idEmissor);
			$stmt1->execute();
			$result = $stmt1->fetch();

	        $finalArray[] = array(
	        	'IDNotificacao' => $row['idnot'],
	            'Data'  => $row['data'],
	            'IDSeguidor' => $row['idseguidor'],
	            'Email' => $result['email'],
	            'NomeSeguidor' => $result['nome']);
	    }
	    
	    return $finalArray;
	}

	function getNewUpvotes($idUser){
		global $conn;

		$stmt = "SELECT 
		notificacao.id AS idnot, 
		upvote.iduser AS idupvoter, 
		upvote.data AS data, citacao.id AS idcitacao, 
		citacao.fonte AS fonte, 
		citacao.textoquote AS textoquote 
					FROM upvote, notificacao, citacao
					WHERE citacao.iduser = :id
					AND notificacao.iduser = :id
					AND upvote.idcitacao = citacao.id
					AND notificacao.upvote = upvote.id";
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':id', $idUser);
		$stmt->execute();

		$finalArray = array();

	    while ($row = $stmt->fetch() ) {
	    	

	    	$stmt1 = "SELECT nome, email FROM utilizador 
				WHERE utilizador.id = :idUp";
			$stmt1 = $conn->prepare($stmt1);
			$stmt1->bindParam(':idUp', $row['idupvoter']);
			$stmt1->execute();
			$result = $stmt1->fetch();

	        $finalArray[] = array(
	        	'IDNotificacao' => $row['idnot'],
	            'Data'  => $row['data'],
	            'IDUpvoter' => $row['idupvoter'],
	            'Email' => $result['email'],
	            'NomeUpvoter' => $result['nome'],
	            'IDCitacao' => $row['idcitacao'],
	            'Citacao' => $row['textoquote'],
	            'FonteCitacao' => $row['fonte']);
	    }
	    
	    return $finalArray;
	}

	function removeNotification($id){
		global $conn;
		$stmt = $conn->prepare("DELETE FROM notificacao WHERE notificacao.id = :id");
		$stmt->bindParam(':id', $id);
		return $stmt->execute();
	}


?>