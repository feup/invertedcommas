<?php
	
	function searchTokenQuoteAndSource($str, $src, $idUser){
		global $conn;


		$stmt = $conn->prepare('SELECT citacao.id AS idcitacao, citacao.data , citacao.textoquote , citacao.fonte , utilizador.nome , utilizador.id, utilizador.email 
			FROM citacao LEFT JOIN utilizador ON (citacao.iduser = utilizador.id) WHERE
			citacao.textoquote LIKE :str
			AND
			citacao.fonte LIKE :src
			AND
			(
			citacao.iduser IN (SELECT amizade.iduser1 FROM amizade WHERE amizade.iduser2 = :iduser1 )
			OR
			citacao.iduser IN (SELECT amizade.iduser2 FROM amizade WHERE amizade.iduser1 = :iduser2)
			OR
			citacao.iduser IN (SELECT seguidor.idseguido FROM seguidor WHERE seguidor.idseguidor = :iduser3)
			OR
			citacao.iduser IN (SELECT utilizador.id FROM utilizador WHERE utilizador.privperfil = 1))');

		$str= "%" . $str. "%";
		$src= "%" . $src. "%";
		$stmt->bindParam(':str', $str, PDO::PARAM_STR);
		$stmt->bindParam(':src', $src, PDO::PARAM_STR);
		$stmt->bindParam(":iduser1",$idUser);
		$stmt->bindParam(":iduser2",$idUser);
		$stmt->bindParam(":iduser3",$idUser);
		$stmt->execute();


	 	$finalArray = array();

	    while ($row = $stmt->fetch() ) {


			$stmt1 = $conn->prepare("SELECT COUNT(*) AS contador FROM upvote, citacao WHERE
									upvote.idcitacao = citacao.id AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$resultA = $stmt1->fetch();

			$stmt1 = $conn->prepare("SELECT * FROM upvote, utilizador, citacao WHERE
									upvote.idcitacao = citacao.id AND
									upvote.iduser = :userParam AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":userParam", $idUser);
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$mayUpvote = !($stmt1->fetch());


	        $finalArray[] = array(
	        	'userID'    => $row['id'],
	            'User'  	=> $row['nome'],
	            'IDCitacao' => $row['idcitacao'],
	            'Texto' 	=> $row['textoquote'],
	            'Fonte' 	=> $row['fonte'],
	            'Upvotes' 	=> $resultA['contador'],
	            'PodeUp'	=> $mayUpvote,
	            'Data' 		=> $row['data'],
	            'Email'		=> $row ['email']);
	    }

	    return $finalArray;

	}

	

	function searchTokenUserName($str){
		global $conn;

		$str= "%" . $str. "%";
		$stmt = 'SELECT * FROM utilizador WHERE nome LIKE :str';
		$stmt = $conn->prepare($stmt);
		$stmt->bindParam(':str', $str, PDO::PARAM_STR);
		$stmt->execute();


		$array = array();
		while ($row = $stmt->fetch()) {
	        $array[] = array(
	            'ID' => $row['id'],
	            'Nome' => $row['nome'],
	            'Email' => $row['email'],
	            'PrivPerfil' => $row['privperfil'],
	            'PrivAmigos' => $row['privamigos'],
	            'PrivSeguidores' => $row['privseguidores']);
	    }

	    return $array;
	}


	function searchTokenQuote($str, $idUser){
		global $conn;


		$stmt = $conn->prepare('SELECT citacao.id AS idcitacao, citacao.data , citacao.textoquote , citacao.fonte , utilizador.nome , utilizador.id, utilizador.email 
			FROM citacao LEFT JOIN utilizador ON (citacao.iduser = utilizador.id) WHERE
			citacao.textoquote LIKE :str
			AND
			(
			citacao.iduser IN (SELECT amizade.iduser1 FROM amizade WHERE amizade.iduser2 = :iduser1 )
			OR
			citacao.iduser IN (SELECT amizade.iduser2 FROM amizade WHERE amizade.iduser1 = :iduser2)
			OR
			citacao.iduser IN (SELECT seguidor.idseguido FROM seguidor WHERE seguidor.idseguidor = :iduser3)
			OR
			citacao.iduser IN (SELECT utilizador.id FROM utilizador WHERE utilizador.privperfil = 1))');

		$str= "%" . $str. "%";
		$stmt->bindParam(':str', $str, PDO::PARAM_STR);
		$stmt->bindParam(":iduser1",$idUser);
		$stmt->bindParam(":iduser2",$idUser);
		$stmt->bindParam(":iduser3",$idUser);
		$stmt->execute();


	 	$finalArray = array();

	    while ($row = $stmt->fetch() ) {


			$stmt1 = $conn->prepare("SELECT COUNT(*) AS contador FROM upvote, citacao WHERE
									upvote.idcitacao = citacao.id AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$resultA = $stmt1->fetch();

			$stmt1 = $conn->prepare("SELECT * FROM upvote, utilizador, citacao WHERE
									upvote.idcitacao = citacao.id AND
									upvote.iduser = :userParam AND
									citacao.id = :idCitacao");
			$stmt1->bindParam(":userParam", $idUser);
			$stmt1->bindParam(":idCitacao", $row['idcitacao']);
			$stmt1->execute();
			$mayUpvote = !($stmt1->fetch());


	        $finalArray[] = array(
	            'User'  	=> $row['nome'],
	            'IDCitacao' => $row['idcitacao'],
	            'Texto' 	=> $row['textoquote'],
	            'Fonte' 	=> $row['fonte'],
	            'Upvotes' 	=> $resultA['contador'],
	            'PodeUp'	=> $mayUpvote,
	            'Data' 		=> $row['data'],
	            'Email'		=> $row ['email']);
	    }

	    return $finalArray;

	}

?>