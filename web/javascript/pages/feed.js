
$('a.post-tag').click(function(){
  
  var $this = $(this);
  var offset = $this.offset();
  var thisW = $(this).outerWidth(); 
  var myPos = {X:offset.left+thisW-300, Y:offset.top+26}; 
  $('#tag-menu').css({left:myPos.X, top:myPos.Y, width:300, height:200}).toggle();
});


setTimeout('hide_message()', 3000);

function hide_message()
{
	//var div = document.getElementById("success_messages");
	//div.parentNode.removeChild(div);
	$("#success_messages").fadeOut(1000, function() { $(this).remove(); });
	$("#error_messages").fadeOut(1000, function() { $(this).remove(); });
}

function feedshow(data){
	//$(document).ready(function(){
		$('#completefeed').remove();
		var receivedData = JSON.parse(data);
		//console.log(receivedData.FeedPosts)
		var html='<div id="completefeed">';
		
		for (var i in receivedData.FeedPosts){
				
				var aux = receivedData.FeedPosts[i];
				
			   	html+= '<div class="container-fluid"> <span class="span8" style="margin-left: 0;background-color: rgba(0,0,0, .15) !important;margin-bottom: 12px;padding-top: 8px;';
				html+= 'padding-bottom: 8px;"><div  style="margin-left: 8px;">';
				html+= '<div class="media" >';
				html+='<div style="height: 64px;width: 64px;float: left;margin-top: 14px;">';
				html+='<a class="pull-left" href="#">';
				if(aux.PodeUp == true){
					html+='<span class="label mayupvote" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
					html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
				}
				else if(aux.PodeUp == false){
					html+='<span class="label label-success alreadyupvoted" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
					html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
				}
				html+='<div class="arrowdiv"><i style="arrowi"></i><u class="arrowu_green"></u></div>';
				html+='<br>';
				html+='<div class = "QuoteOptions">';
				html+='<a href="https://twitter.com/share" class="twitter-share-button" data-text="&quot;' + aux.Texto + '&quot;(' + aux.Fonte + ')" data-hashtags="InvertedCommas" data-url="false" data-lang="en"></a>  <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>'
				html+='<!--div class="fb-share-button" style="background-image: linear-gradient(#fff,#dedede);border: #ccc solid 1px;border-radius: 3px;width:59px;" href="http://www.facebook.com/share.php?u=<;url>" onclick="return fbs_click()"></div--> ';
				html+='<br><br>';
				html+='</div>';
				html+='</div>';
				html+='<a class="magnificPopup imgBorder_inner" style="height: 64px;float: left;width: 64px;margin-right: 10px;" data-title="" target="_self"'+'onclick=loadNewProfile('+aux.userID+')>';
				html+='<span class="overlay zoom"></span>';
				html+='<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) +'?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic" style="height: 64px;float: left;width: 64px;"'+'onclick=loadNewProfile('+aux.userID+')>';
				html+='</a>';
				html+='<div class="QuoteText" style="margin-top: 7px;">';
				html+=aux.Texto;
				html+='</div>';
				html+='<div class="media-body">';
				html+='<h4 class="media-heading" style="font-size: 12px;margin-top: 6px;opacity: 0.5;">-- ' + aux.Fonte + '</h4>';
				html+='</div>';
				html+='<br>';
				html+='</div>';
				html+='</div> </span></div>';
		}
		html+='</div>';
		$( "#completepage" ).append(html);
		upvoteButtonHandlerFeed();
		alreadyUpvotedButtonHandlerFeed();

	//});
}


function feedupdate(){
	$.ajax({             
		type: 'post',
		url: '../../api/loadfeed.php',
		success: function (data) {
			feedshow(data);
		}
	});


}

function upvoteButtonHandlerFeed(){
	$( ".mayupvote" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		var clicked = $(this);
		//var dataToSend = "{idcitacao:'" + idcit + "'}";
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'GET',
			url: '../../actions/quotes/upvote.php',
			data: dataToSend,
			success: function (data) {
				/*
				var receivedData = JSON.parse(data);
				var quoteinfo = receivedData.quoteinfo;
				var html = '<i class="fa fa-arrow-up" id="'+ quoteinfo.id + '"></i> ' + receivedData.upvotes;
				clicked.attr("class","label label-success pista alreadyupvoted");
				clicked.empty();
				clicked.append(html);
				*/
				feedupdate();
			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
		});
		
	})
}

function alreadyUpvotedButtonHandlerFeed(){
	$( ".alreadyupvoted" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'get',
			url: '../../actions/quotes/unupvote.php',
			data: dataToSend,
			success: function (data) {
				feedupdate();
			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
		});
		
	})
}