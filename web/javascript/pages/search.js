$(document).ready(function() {
  addQuote();
});

function addQuote() {
  $('#searchform').submit(function (e) {
      
      e.preventDefault();
      //var addquote = $('#addquote').val();
      //var addsource = $('#addsource').val();
      //console.log(Array(addquote,addsource));
      
      var a = $( "#searchthings" ).val();
      

      var pattquote = new RegExp("quote\\s\\w+$");
      var pattquotefont = new RegExp("quote\\s\\w+\\sfrom\\s\\w+$");
      var pattuser = new RegExp("user\\s\\w+$");

      var foundquote = pattquote.test(a);
      var foundquotefont = pattquotefont.test(a);
      var founduser = pattuser.test(a);

      
      $('#sr').click();

      if(founduser)
      {
          var user = a.substring(a.indexOf(' ')+1, a.length);
          getUsers(user);
          
       
      }
      if(foundquotefont)
      { 
          a = a.substring(a.indexOf(' ')+1,a.length);
          var quote = a.substring(0,a.indexOf(' '));
          a = a.substring(a.indexOf(' ')+1,a.length);
          a = a.substring(a.indexOf(' ')+1,a.length);
          var font = a;
          getQuoteswithFont(quote,font);
     
      }
      if(foundquote)
      {
          var quote = a.substring(a.indexOf(' ')+1, a.length);
          getQuotes(quote);
      }
      if(!founduser && !foundquotefont && !foundquote)
      {
        showhelp();
      }
      
  });

  $("#searchthings").focus(function(){

          if(this.value == 'Search...') 
                {this.value = '';}
  })
  .blur(function(){
          if (this.value == '') 
                {this.value = 'Search...';}
  });

}

function showuser(data)
{
  $("#totalpage2").remove();
  var html='<div id=totalpage2>';
  html+='      <div class="row-fluid"><div class="span12" ><div class="heading_stroke"><div class="heading_stroke_wrapper"><div class="stroke-text"><h4>Users</h4></div>';
  html+='<div class="stroke-holder"><div class="stroke-line"></div></div></div></div></div></div>';

    var receivedData = JSON.parse(data);
    
    console.log(receivedData.result);
    for(var i in receivedData.result){

      var aux = receivedData.result[i];
      html+='<div class="row-fluid">';
      html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title=""target="_self" '+'onclick="loadNewProfile('+aux.ID+')">';
      html += '<span class="overlay zoom"></span>';
      html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
      html += 'alt="image text" style="width: 64px;height: 64px;">';
      html += '</a>';
      html+='<div class="span9">';
      html+='<h6 class="stroke-text upperCase bold_weight span3">Username:</h6><br>';
      html+='<h6 style="float:left;position: fixed;margin-left: 0;" class=" stroke-text span6">'+aux.Nome+'</h6>';
      html+='</div>';
      html +='</div>';
      //html += '<div>'+ aux.Nome + ' ' + aux.Email + ' css me plz :( </div><br><br><br>';
    }

  html+='</div>';

  $("#totalpage").append(html);

}

function showquote(data) 
{
  $("#totalpage2").remove();
  var html='<div id=totalpage2>';
html+='      <div class="row-fluid"><div class="span12" ><div class="heading_stroke"><div class="heading_stroke_wrapper"><div class="stroke-text"><h4>Quotes</h4></div>';
  html+='<div class="stroke-holder"><div class="stroke-line"></div></div></div></div></div></div>';

  
    var receivedData = JSON.parse(data);
    
    console.log(receivedData.result);

    

   
    for(var i in receivedData.result){

      var aux = receivedData.result[i];

      html+='<div class="row-fluid">';
        html+= '<div class="container-fluid"> <span class="span12" style="margin-left: 0;background-color: rgba(0,0,0, .15) !important;margin-bottom: 12px;padding-top: 8px;';
        html+= 'padding-bottom: 8px;"><div  style="margin-left: 8px;">';
        html+= '<div class="media" >';
        html+='<div style="height: 64px;width: 64px;float: left;margin-top: 14px;">';
        html+='<a class="pull-left" href="#">';
        if(aux.PodeUp == true){
          html+='<span class="label mayupvote" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
          html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
        }
        else if(aux.PodeUp == false){
          html+='<span class="label label-success alreadyupvoted" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
          html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
        }
        html+='<div class="arrowdiv"><i style="arrowi"></i><u class="arrowu_green"></u></div>';
        html+='<br>';
        html+='<div class = "QuoteOptions">';
        html+='<a href="https://twitter.com/share" class="twitter-share-button" data-text="&quot;' + aux.Texto + '&quot;(' + aux.Fonte + ')" data-hashtags="InvertedCommas" data-url="false" data-lang="en"></a><script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>'
        html+='<!--div class="fb-share-button" style="background-image: linear-gradient(#fff,#dedede);border: #ccc solid 1px;border-radius: 3px;width:59px;" href="http://www.facebook.com/share.php?u=<;url>" onclick="return fbs_click()"></div--> ';
        html+='<br><br>';
        html+='</div>';
        html+='</div>';
        html+='<a class="magnificPopup imgBorder_inner" style="height: 64px;float: left;width: 64px;margin-right: 10px;" data-title="" target="_self"'+'onclick=loadNewProfile('+aux.userID+')>';
        html+='<span class="overlay zoom"></span>';
        html+='<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) +'?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic" style="height: 64px;float: left;width: 64px;"'+'onclick=loadNewProfile('+aux.userID+')>';
        html+='</a>';
        html+='<div class="QuoteText" style="margin-top: 7px;">';
        html+=aux.Texto;
        html+='</div>';
        html+='<div class="media-body">';
        html+='<h4 class="media-heading" style="font-size: 12px;margin-top: 6px;opacity: 0.5;">-- ' + aux.Fonte + '</h4>';
        html+='</div>';
        html+='<br>';
        html+='</div>';
        html+='</div> </span></div>';
        html+='</div>';
    }



  
  
  html+='</div>';
  


  $("#totalpage").append(html);

}


function showhelp()
{

  $("#totalpage2").remove();
  var html='<div id="totalpage2"><h3>How to use our search engine:</h3>';
  
    
  html+='<div class="row-fluid" style="margin-bottom: 12px;">';
html+='<div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">';
html+='<h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" >Searching for Users:</h7>';
html+='<h6 style="padding-left: 34px;">';
html+='user (string)';
html+='</h6>';
html+='<hr>';
html+='</div>';
html+='</div>';

html+='<div class="row-fluid" style="margin-bottom: 12px;">';
html+='<div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">';
html+='<h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" > Searching for Quotes:</h7>';
html+='<h6 style="padding-left: 34px;">';
html+='quote (string)';
html+='</h6>';
html+='<hr>';
html+='</div>';
html+='</div>';

html+='<div class="row-fluid" style="margin-bottom: 12px;">';
html+='<div class="span8" style="background-color: rgba(27,27,27, .5) !important;border-radius: 7px;height: 75px;">';
html+='<h7 class="upperCase bold-weight stroke-text" style="font-weight: 900;padding: 0px 20px 0px 20px;padding-top: 12px;" > Searching for Quotes from a specific Author:</h7>';
html+='<h6 style="padding-left: 34px;">';
html+='quote (string) from (string)';
html+='</h6>';
html+='<hr>';
html+='</div>';
html+='</div>';


  html+='</div>';
  


  $("#totalpage").append(html);

}

function getUsers(userId)
{
    var dataToSend = userId;
    $.ajax({             
      type: 'GET',
      url: '../../actions/search/searchForUsers.php?str='+dataToSend,
      success: function (data) {
        
        showuser(data);
      },
      error: function (xhr, status, error) {
        alert(status);
        alert(xhr.responseText);
      }
    });
}


function getQuotes(quoteContent)
{
    var dataToSend = quoteContent;
    $.ajax({             
      type: 'GET',
      url: '../../actions/search/searchForQuotes.php?str='+dataToSend,
      success: function (data) {
        showquote(data);
      },
      error: function (xhr, status, error) {
        alert(status);
        alert(xhr.responseText);
      }
    });
}


function getQuoteswithFont(quoteContent,font)
{
    
    $.ajax({             
      type: 'GET',
      url: '../../actions/search/searchForQuotesAndSource.php?str='+quoteContent+'&src='+font,
      success: function (data) {
        showquote(data);
      },
      error: function (xhr, status, error) {
        alert(status);
        alert(xhr.responseText);
      }
    });
}