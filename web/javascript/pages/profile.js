setTimeout('hide_message()', 3000);

function hide_message()
{
	//var div = document.getElementById("success_messages");
	//div.parentNode.removeChild(div);
	$("#success_messages").fadeOut(1000, function() { $(this).remove(); });
	$("#error_messages").fadeOut(1000, function() { $(this).remove(); });
}


function profileshow(data){
		friendsUpdate();
		followersUpdate();
		followingUpdate();
		
		$("#completeprofile").remove();
		var receivedData = $.parseJSON(data)//JSON.parse(data);
		var html='<div id="completeprofile">';
		if (receivedData == null){return;}
		for (var i in receivedData.ProfilePosts){

			var aux = receivedData.ProfilePosts[i];

			html+= '<span class="acc_content span9" style="margin-left: 0;margin-left: 12px;background-color: rgba(0,0,0, .15) !important;margin-bottom: 5px;width: 100%;">';
			html+= '<div class="span12" style="margin-left: 8px;"> <div class="media" style="width:100%">';
			html+= '<div style="height: 64px;width: 64px;float: left;margin-top: 6px;"> <a class="pull-left" href="#">';

			if(aux.PodeUp == true){
				html+='<span class="label mayupvote" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
				html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
			}
			else if(aux.PodeUp == false){
				html+='<span class="label label-success alreadyupvoted" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
				html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
			}

			html+= '<br> <span class="label label-important deletebutton" style="width: 45px;text-align: center;line-height: 10px;height: 11px;" alt="Delete Quote">';
			html+= '<i alt="Delete Quote" class="fa fa-times" id="'+ aux.IDCitacao + '"></i></span></a></div>';
			html+= '<div class="QuoteText" style="margin-top: 7px;">' + aux.Texto;
			html+= '</div><div class="media-body"> <h4 class="media-heading" style="font-size: 12px;margin-top: 6px;opacity: 0.5;">-- ';
			html+= aux.Fonte + '</h4></div><br></div></div><div class = "QuoteOptions" style="width: 61px;margin-left: 8px;">';
			html+= '<a href="https://twitter.com/share" class="twitter-share-button" data-text="&quot;' + aux.Texto + '&quot;(' + aux.Fonte + ')" data-hashtags="InvertedCommas" data-url="false" data-lang="en"></a><script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>';
			html+= '<br><br></div></span>';

		}
		html+='</div>';
		$( "#profilequotes" ).append(html);
		
		upvoteButtonHandlerProfile();
		alreadyUpvotedButtonHandlerProfile();
		deleteButtonHandlerProfile();


		//traf();
	//});
}


function profileupdate(){
	$.ajax({             
		type: 'POST',
		url: '../../api/loadquotes.php',
		success: function (data) {
			profileshow(data);
		}
	});
}


function upvoteButtonHandlerProfile(){
	$( ".mayupvote" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		//var dataToSend = "{idcitacao:'" + idcit + "'}";
		var dataToSend = "idcitacao=" + idcit;
		//alert(dataToSend);
		$.ajax({             
			type: 'GET',
			url: '../../actions/quotes/upvote.php',
			data: dataToSend,
			success: function (data) {
				profileupdate();
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		
	});
}

function alreadyUpvotedButtonHandlerProfile(){
	$( ".alreadyupvoted" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'get',
			url: '../../actions/quotes/unupvote.php',
			data: dataToSend,
			success: function (data) {
				profileupdate();
				
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		
	});
}


function deleteButtonHandlerProfile(){
	$( ".deletebutton" ).click(function(e){
		var idcit =  $(this).children("i").attr("id");
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'get',
			url: '../../actions/quotes/delete.php',
			data: dataToSend,
			success: function (data) {
				profileupdate();
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		
	});
}


// Friends TAB

function showfriends(data){
		$("#compfriends").remove();
		var receivedData = JSON.parse(data);
		var html='<div id="compfriends">';
		if (receivedData == null){return;}
		for(var i in receivedData.Friends){

			var aux = receivedData.Friends[i];

			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title=" "target="_self"'+'onclick="loadNewProfile('+aux.ID+')">';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;">';
			html += '</a>';

		}
		html += '</div>';
		$( "#tab_bottom_friends" ).append(html);
}

function friendsUpdate(){
	$.ajax({             
		type: 'post',
		url: '../../api/getfriends.php',
		success: function (data) {
			showfriends(data);
		}
	});
}

// Follower TAB
function showfollowers(data){

		$("#compfollowers").remove();
		var receivedData = $.parseJSON(data)//JSON.parse(data);
		
		var html='<div id="compfollowers">';
		if (receivedData == null){return;}
		for (var i in receivedData.Followers){
			var aux = receivedData.Followers[i];
			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;"   target="_self"'+'onclick="loadNewProfile('+aux.ID+')">';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;" '+' onclick="loadNewProfile('+aux.ID+')" >';
			html += '</a>';
		}
		html += '</div>';
		$( "#tab_bottom_followers" ).append(html);
}


function followersUpdate(){
	$.ajax({             
		type: 'post',
		url: '../../api/getfollowers.php',
		success: function (data) {
			showfollowers(data);
		}
	});
}


// Following TAB
function showfollowing(data){
	//$(document).ready(function(){
		$("#compfollowing").remove();
		var receivedData = $.parseJSON(data)//JSON.parse(data);
		var html='<div id="compfollowing">';
		if (receivedData == null){return;}
		for (var i in receivedData.Following){
			var aux = receivedData.Following[i];

			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title="" target="_self"'+'onclick="loadNewProfile('+aux.ID+')">';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;">';
			html += '</a>';
		}
		html += '</div>';
		$( "#tab_bottom_following" ).append(html);
	//});

}

function followingUpdate(){
	$.ajax({             
		type: 'post',
		url: '../../api/getfollowing.php',
		success: function (data) {
			showfollowing(data);		
		}
	});
}
