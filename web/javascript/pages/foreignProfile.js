function profileshowForeign(id,data){
	
	$('#foreignProfile #completeprofile').remove();
	var receivedData = data;
	var html='<div id="completeprofile">';
	for (var i in receivedData.ProfilePosts){

		var aux = receivedData.ProfilePosts[i];

		html+= '<span class="acc_content span9" style="margin-left: 0;margin-left: 12px;background-color: rgba(0,0,0, .15) !important;margin-bottom: 5px;width: 100%;">';
		html+= '<div class="span12" style="margin-left: 8px;"> <div class="media" style="width:100%">';
		html+= '<div style="height: 64px;width: 64px;float: left;margin-top: 6px;"> <a class="pull-left" href="#">';

		if(aux.PodeUp == true){
			html+='<span class="label mayupvoteforeign" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
			html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
		}
		else if(aux.PodeUp == false){
			html+='<span class="label label-success alreadyupvotedforeign" style="width: 45px;text-align: center;line-height: 10px;height: 11px;">';
			html+='<i class="fa fa-arrow-up" id="'+ aux.IDCitacao + '"></i> ' + aux.Upvotes + '</span>';
		}

		html+= '</span></a></div>';
		html+= '<div class="QuoteText" style="margin-top: 7px;">' + aux.Texto;
		html+= '</div><div class="media-body"> <h4 class="media-heading" style="font-size: 12px;margin-top: 6px;opacity: 0.5;">-- ';
		html+= aux.Fonte + '</h4></div><br></div></div><div class = "QuoteOptions" style="width: 61px;margin-left: 8px;">';
		html+= '<a href="https://twitter.com/share" class="twitter-share-button" data-text="&quot;' + aux.Texto + '&quot;(' + aux.Fonte + ')" data-hashtags="InvertedCommas" data-url="false" data-lang="en"></a><script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>';
		html+= '<br><br></div></span>';

	}
	html+='</div>';
	$( "#foreignProfile #profilequotes" ).append(html);
	
	$("#fp").click();
	upvoteButtonHandlerForeign(id);
	alreadyUpvotedButtonHandlerForeign(id);

}

function loadNewProfile(id){	
	var urlStr = '../../api/getProfile.php';	
	if (id != null){
		urlStr += "?userid="+id.toString();
	}

	$.ajax({             
		type: 'post',
		url: urlStr,
		success: function (data) {

			foreignfriendsUpdate(id);
			foreignfollowingUpdate(id);
			foreignfollowersUpdate(id);
			

			foreignInteractionUpdate(id);
			var obj=JSON.parse(data);

			$("#foreignProfile #usernameProfile").text("");

			$("#foreignProfile #usernameProfile").text(obj.username+"'s Profile");

			profileshowForeign(id,obj.quotes);

			$("#foreignProfile #profileBio").text(obj.bio);
			$("#foreignProfile #profilepicPhoto").attr("src","http://www.gravatar.com/avatar/"+obj.md5email.toString()+"?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png");
			//console.log("http://www.gravatar.com/avatar/"+obj.md5email.toString()+"?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png");

		}
	});
}



function foreignshowfriends(id,data){
	//$(document).ready(function(){
		$('#compfriends').remove();
		var receivedData = JSON.parse(data);
		var html='<div id="compfriends">';
		for(var i in receivedData.Friends){

			var aux = receivedData.Friends[i];

			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title="" target="_self"'+'onclick=loadNewProfile('+aux.userID+')>';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;">';
			html += '</a>';

		}
		html += '</div>';
		$( "#ftab_bottom_friends" ).append(html);

		$( ".fnofriends").empty();
		$( ".fnofriends").append("Friends (" + receivedData.NumberFriends + ")");
	//});

}

function foreignfriendsUpdate(id){
	$.ajax({             
		type: 'get',
		url: '../../api/getfriendsbyuser.php',
		data: 'id='+id,
		success: function (data) {
			foreignshowfriends(id,data);
		}
	});
}

// Follower TAB
function foreignshowfollowers(id,data){
	//$(document).ready(function(){
		$('#compfollowers').remove();
		var receivedData = JSON.parse(data);
		var html='<div id="compfollowers">';
		for (var i in receivedData.Followers){
			var aux = receivedData.Followers[i];
			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title=""  target="_self"'+'onclick=loadNewProfile('+aux.userID+')>';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;">';
			html += '</a>';
		}
		html += '</div>';
		$( "#ftab_bottom_followers" ).append(html);


		$( ".fnofollowers").empty();
		$( ".fnofollowers").append("Followers (" + receivedData.NumberFollowers + ")");
	//});

}


function foreignfollowersUpdate(id){
	$.ajax({             
		type: 'get',
		url: '../../api/getfollowersbyuser.php',
		data: 'id='+id,
		success: function (data) {
			foreignshowfollowers(id,data);
		}
	});
}


// Following TAB
function foreignshowfollowing(id,data){
	//$(document).ready(function(){
		$('#compfollowing').remove();
		var receivedData = JSON.parse(data);
		var html='<div id="compfollowing">';

		for (var i in receivedData.Following){
			var aux = receivedData.Following[i];

			html += '<a class="magnificPopup imgBorder_inner" style="margin-bottom: 11px;height: 64px;float: left;width: 64px;margin-right: 10px;" data-title="" target="_self"'+'onclick=loadNewProfile('+aux.userID+')>';
			html += '<span class="overlay zoom"></span>';
			html += '<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) + '?s=153&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png';
			html += 'alt="image text" style="width: 64px;height: 64px;">';
			html += '</a>';


		}
		html += '</div>';
		$( "#ftab_bottom_following" ).append(html);

		$( ".fnofollowing").empty();
		$( ".fnofollowing").append("Following (" + receivedData.NumberFollowing + ")");
	//});

}

function foreignfollowingUpdate(id,data){
	$.ajax({             
		type: 'get',
		url: '../../api/getfollowingbyuser.php',
		data: 'id='+id,
		success: function (data) {
			foreignshowfollowing(id,data);		
		}
	});
}


function upvoteButtonHandlerForeign(id){
	$( ".mayupvoteforeign" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		//var dataToSend = "{idcitacao:'" + idcit + "'}";
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'GET',
			url: '../../actions/quotes/upvote.php',
			data: dataToSend,
			success: function (data) {
				loadNewProfile(id);
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		
	});
}

function alreadyUpvotedButtonHandlerForeign(id){
	$( ".alreadyupvotedforeign" ).click(function(){
		var idcit =  $(this).children("i").attr("id");
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'get',
			url: '../../actions/quotes/unupvote.php',
			data: dataToSend,
			success: function (data) {
				loadNewProfile(id);
				
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		
	});
}


function foreignInteractionUpdate(id){
	$.ajax({             
		type: 'post',
		url: '../../api/getInteractionsInfo.php',
		data: "idOtherUser=" + id,
		success: function (data) {
			foreignInteractionButtons(id,data);
		}
	});
}

// TODO: TA SEMPRE COM O ID 17!
function foreignInteractionButtons(id,data) {
	//$(document).ready(function(){

		var receivedData = JSON.parse(data);

		$("#userInteraction").empty();
		var html;
		if(receivedData.IsFriend == "true"){
			html = '<button class="addfriend" id="btn_removefriend"><i class="fa fa-users"></i> Remove</button>';
			$( "#userInteraction" ).append(html);
		}
		else if(receivedData.IsFriend == "false"){
			html = '<button class="addfriend" id="btn_addfriend"><i class="fa fa-users"></i> Add Friend</button>';
			$( "#userInteraction" ).append(html);
		}
		else if(receivedData.IsFriend == "pending"){
			html = '<button class="addfriend" id="btn_pending"><i class="fa fa-users"></i> Requested</button>';
			$( "#userInteraction" ).append(html);
		}

		if(receivedData.IsFollowing == true){
			html = '<button class="followuser" id="btn_unfollowuser"><i class="fa fa-eye"></i> Unfollow</button>';
			$( "#userInteraction" ).append(html);
		}
		else if(receivedData.IsFollowing == false){
			html = '<button class="followuser" id="btn_followuser"><i class="fa fa-eye"></i> Follow</button>';
			$( "#userInteraction" ).append(html);
		}

		foreignTopButtonsHandler(id);
	//})}
}


function foreignTopButtonsHandler(id){
	foreignaddFriendButtonHandler(id);
	foreignremoveFriendButtonHandler(id);
	foreignunfollowUserButtonHandler(id);
	foreignfollowUserButtonHandler(id);
	foreignrequestedButtonHandler(id);
}


function foreignaddFriendButtonHandler(id) {
	$("#btn_addfriend").click(function(e){
		e.preventDefault();		
		$.ajax({             
			type: 'post',
		//TODO : 17 
		url: '../../actions/users/addfriendrequest.php',
		data: "idUser=" + id,
		success: function (data) {
			foreignInteractionUpdate(id);
		}
	});
	}); 
}

function foreignrequestedButtonHandler(id) {

	$("#btn_pending").click(function(e){
		e.preventDefault();		
		$.ajax({             
			type: 'post',
		//TODO : 17 
		url: '../../actions/users/removefriendrequest.php',
		data: "idUser=" + id,
		success: function (data) {
			foreignInteractionUpdate(id);
		}
	});
	}); 
}

function foreignremoveFriendButtonHandler(id) {
	$("#btn_removefriend").click(function(e){
		e.preventDefault();

		$.ajax({             
			type: 'post',
			url: '../../actions/users/removefriend.php',
			data: "idUser=" + 17,
			success: function (data) {
				foreignInteractionUpdate(id);
			}

		});
	});
}

function foreignunfollowUserButtonHandler(id) {
	$("#btn_unfollowuser").click(function(e){

		e.preventDefault();

		$.ajax({             
			type: 'post',
			url: '../../actions/users/unfollow.php',
			data: "idUser=" + id,
			success: function (data) {
				foreignInteractionUpdate(id);
			}
		});

	});
}

function foreignfollowUserButtonHandler(id) {
	$("#btn_followuser").click(function(e){
		e.preventDefault();

		$.ajax({             
			type: 'post',
			url: '../../actions/users/follow.php',
			data: "idUser=" + id,
			success: function (data) {
				foreignInteractionUpdate(id);
			}
		});
	});
}
