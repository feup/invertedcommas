$(document).ready(function(){
	
	



	$.ajax({             
			type: 'GET',
			url: '../../api/getNumberNotifications.php',
			success: function (data) {
				var receivedData = JSON.parse(data);
				refreshNotificationNumber(receivedData);
				
			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});


});

function closeNoti(typeNoti) {

/*	typeNoti - tipo notificao ex: friends,followers,upvotes
	eraseData - limpar registo notificacoes ex: true,false */

	if(typeNoti == "friends") {
		$('.show_notificationsFriends').css('visibility', 'hidden');
}
	if(typeNoti == "followers") {
		$('.show_notificationsFollowers').css('visibility', 'hidden');
}
	if(typeNoti == "upvotes") {
		$('.show_notificationsUpvotes').css('visibility', 'hidden');
}
		$.ajax({             
				type: 'GET',
				url: '../../api/getNumberNotifications.php',
				success: function (data) {
					var receivedData = JSON.parse(data);
					refreshNotificationNumber(receivedData);
					
				},
				error: function (xhr, status, error) {
	                alert(status);
	                alert(xhr.responseText);
	            }
		});
}


function refreshNotificationNumber(data){
	if(data.Friends == 0){
		
		var html = '<i class="fa fa-users"></i>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'

		$('.friendnumbernotif').empty();
		$('.friendnumbernotif').append(html);
		$('.friendnumbernotif').attr('class', 'box friend notnew friendnumbernotif');
	}
	else{
		var html = '<a href="#" onclick="showNotificationFriends()"><i class="fa fa-users"></i></a>'
		html +='<div class="bubble">' + data.Friends + '</div>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'

		$('.friendnumbernotif').empty();
		$('.friendnumbernotif').append(html);
		$('.friendnumbernotif').attr('class', 'box friend new friendnumbernotif');

	}

	if(data.Followers == 0){
		
		var html = '<i class="fa fa-eye"></i>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'

		$('.follownumbernotif').empty();
		$('.follownumbernotif').append(html);
		$('.follownumbernotif').attr('class', 'box follower notnew follownumbernotif');
	}
	else{
		var html = '<a href="#" onclick="showNotificationFollowers()"><i class="fa fa-eye"></i></a>'
		html += '<div class="bubble">' + data.Followers + '</div>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'


		$('.follownumbernotif').empty();
		$('.follownumbernotif').append(html);
		$('.follownumbernotif').attr('class', 'box follower new follownumbernotif');
	}

	if(data.Upvotes == 0){
		
		var html = '<i class="fa fa-quote-right"></i>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'

		$('.upvotenumbernotif').empty();
		$('.upvotenumbernotif').append(html);
		$('.upvotenumbernotif').attr('class', 'box upquote notnew upvotenumbernotif');
	}
	else{
		var html = '<a href="#" onclick="showNotificationUpvotes()"><i class="fa fa-quote-right"></i></a>'
		html += '<div class="bubble">' + data.Upvotes + '</div>'
		html += '<span class="right"></span>'
		html += '<span class="left"></span>'

		$('.upvotenumbernotif').empty();
		$('.upvotenumbernotif').append(html);
		$('.upvotenumbernotif').attr('class', 'box upquote new upvotenumbernotif');

	}
}


function showNotificationFriends() {
	if ($('#show_notificationsFollowers:visible') || $('#show_notificationsUpvotes:visible'))
	{
			closeNoti('followers');closeNoti('upvotes');
	}
	
	$('.show_notificationsFriends').css('visibility', 'visible');
	 
	var mleft = $(window).width();
	$('.show_notificationsFriends').css('margin-left', mleft/2-200);
	// Retira os numeros
	var html = '<i class="fa fa-users"></i>'
	html += '<span class="right"></span>'
	html += '<span class="left"></span>'

	$('.friendnumbernotif').empty();
	$('.friendnumbernotif').append(html);
	$('.friendnumbernotif').attr('class', 'box friend notnew friendnumbernotif');

	// Vais buscar info da notificacao
	$.ajax({             
			type: 'GET',
			url: '../../api/getNotifications.php',
			success: function (data) {
				var receivedData = JSON.parse(data);
				$('.show_notificationsFriends').empty();
				var html = '';
				html+='<br><table id="notiTableFriends"><tbody>';

				for (var i in receivedData.Friends){
				
					var aux = receivedData.Friends[i];

					html+= '<tr ><td colspan="2" id="notiFriendDate" >'+aux.Data.substr(0,16)+'</td></tr>';
					html+='<tr id="notiInfoTR"><td id="notiPicTD"><a class="magnificPopup imgBorder_inner" id="notiFriendPic"  data-title="" href="../../images/avatars/default.png" target="_self">';
					html+='<span class="overlay zoom"></span>';
					html+='<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) +'?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic" style="height: 16px;float: left;width: 16px;">';
					html+='</a></td>';
					html+='<td id="notiFriendName" >User "' + aux.NomeEmissor + '" wants to be your friend!</td></tr>';
					
				}
				
				html+='</tbody></table><a onclick="'+"closeNoti('friends')"+'" id="closeNoti" ><i class="fa fa-times"></i></a>';
				html+='<div id="requestOption"><a id="acceptFriendButton" alt="Accept ' + aux.NomeEmissor + ' as friend?" onclick="acceptFriendRequest('+ aux.IDNotificacao + ')"><i class="fa fa-plus-square"></i></a><a id="rejectFriendButton" alt="Decline ' + aux.NomeEmissor + "'" + 's friend request?" onclick="rejectFriendRequest('+ aux.IDNotificacao + ')"><i class="fa fa-minus-square"></i></a>';
				$('.show_notificationsFriends').append(html);


				$('#notiTableFriends').each(function() {
			            var currentPage = 0;
			            var numPerPage = 2;
			            var $table = $(this);
			            $table.bind('repaginate', function() {
			                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
			            });
			            $table.trigger('repaginate');
			            var numRows = $table.find('tbody tr').length;
			            var numPages = Math.ceil(numRows / numPerPage);
			            var $pager = $('<div class="pager"></div>');
			            for (var page = 0; page < numPages; page++) {
			                $('<span class="page-number"></span>').text(page + 1).bind('click', {
			                    newPage: page
			                }, function(event) {
			                    currentPage = event.data['newPage'];
			                    $table.trigger('repaginate');
			                    $(this).addClass('active').siblings().removeClass('active');
			                }).appendTo($pager).addClass('clickable');

			            }
			            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
        		});

			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});

}

function acceptFriendRequest(IDNotificacao){
	$.ajax({             
			type: 'GET',
			url: '../../actions/users/acceptFriendRequest.php',
			data: 'notif='+IDNotificacao,
			success: function (data) {
				removeNotification(IDNotificacao);	
				closeNoti('friends')

			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});
}

function rejectFriendRequest(IDNotificacao){
	$.ajax({             
			type: 'GET',
			url: '../../actions/users/rejectFriendRequest.php',
			data: 'notif='+IDNotificacao,
			success: function (data) {
				removeNotification(IDNotificacao);
				closeNoti('friends')				
			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});
}

function showNotificationFollowers() {

	if ($('#show_notificationsFriends:visible') || $('#show_notificationsUpvotes:visible'))
	{
			closeNoti('friends');closeNoti('upvotes');
	}

	$('.show_notificationsFollowers').css('visibility', 'visible');
	var mleft = $(window).width();
	$('.show_notificationsFollowers').css('margin-left', mleft/2-200);
	// Retira os numeros
	var html = '<i class="fa fa-eye"></i>'
	html += '<span class="right"></span>'
	html += '<span class="left"></span>'

	$('.follownumbernotif').empty();
	$('.follownumbernotif').append(html);
	$('.follownumbernotif').attr('class', 'box follow notnew follownumbernotif');

	// Vais buscar info da notificacao
	$.ajax({             
			type: 'GET',
			url: '../../api/getNotifications.php',
			success: function (data) {
				var receivedData = JSON.parse(data);
				$('.show_notificationsFollowers').empty();
				var html = '';
				html+='<br><table id="notiTableFollowers"><tbody>';

				for (var i in receivedData.Followers){
				
					var aux = receivedData.Followers[i];

					html+='<tr><td colspan="2" id="notiFriendDate" >'+aux.Data.substr(0,16)+'</td></tr>';
					html+='<tr id="notiInfoTR"><td id="notiPicTD"><a class="magnificPopup imgBorder_inner" style="height: 16px;float: left;width: 16px;margin-right: 10px;" data-title="" href="{$BASE_URL}images/avatars/default.png" target="_self">';
					html+='<span class="overlay zoom"></span>';
					html+='<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) +'?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic" style="height: 16px;float: left;width: 16px;">';
					html+='</a></td>';
					html+='<td id="notiFriendName" >User "'+aux.NomeSeguidor + '" started following you!</td></tr>';
					
					//removeNotification(aux.IDNotificacao);
				}
					html+='</tbody></table><a onclick="'+"closeNoti('followers')"+'" id="closeNoti" ><i class="fa fa-times"></i></a>';
					$('.show_notificationsFollowers').append(html);


					$('#notiTableFollowers').each(function() {
			            var currentPage = 0;
			            var numPerPage = 2;
			            var $table = $(this);
			            $table.bind('repaginate', function() {
			                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
			            });
			            $table.trigger('repaginate');
			            var numRows = $table.find('tbody tr').length;
			            var numPages = Math.ceil(numRows / numPerPage);
			            var $pager = $('<div class="pager"></div>');
			            for (var page = 0; page < numPages; page++) {
			                $('<span class="page-number"></span>').text(page + 1).bind('click', {
			                    newPage: page
			                }, function(event) {
			                    currentPage = event.data['newPage'];
			                    $table.trigger('repaginate');
			                    $(this).addClass('active').siblings().removeClass('active');
			                }).appendTo($pager).addClass('clickable');
			            }
			            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
			            removeNotification(aux.IDNotificacao);
			        });

			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});

}

function showNotificationUpvotes() {
		if ($('#show_notificationsFollowers:visible') || $('#show_notificationsFriends:visible'))
	{
			closeNoti('followers');closeNoti('friends');
	}
	$('.show_notificationsUpvotes').css('visibility', 'visible');
	var mleft = $(window).width();
	$('.show_notificationsUpvotes').css('margin-left', mleft/2-200);
	// Retira os numeros
	var html = '<i class="fa fa-quote-right"></i>'
	html += '<span class="right"></span>'
	html += '<span class="left"></span>'

	$('.upvotenumbernotif').empty();
	$('.upvotenumbernotif').append(html);
	$('.upvotenumbernotif').attr('class', 'box upquote notnew upvotenumbernotif');

	// Vais buscar info da notificacao
	$.ajax({             
			type: 'GET',
			url: '../../api/getNotifications.php',
			success: function (data) {
				var receivedData = JSON.parse(data);

				$('.show_notificationsUpvotes').empty();
				var html = '';
				html+='<br><table id="notiTableUpvotes"><tbody>';

				for (var i in receivedData.Upvotes){
				
					var aux = receivedData.Upvotes[i];

					html+= '<tr ><td colspan="2" id="notiFriendDate" >'+aux.Data.substr(0,16)+'</td></tr>';
					html+='<tr id="notiInfoTR"><td ><a class="magnificPopup imgBorder_inner" style="height: 16px;float: left;width: 16px;margin-right: 10px;" data-title="" href="{$BASE_URL}images/avatars/default.png" target="_self">';
					html+='<span class="overlay zoom"></span>';
					html+='<img src="http://www.gravatar.com/avatar/' + MD5(aux.Email) +'?s=64&d=http%3A%2F%2Fimgur.com%2FSF7eNRt.png" alt="Profile Picture" id="profilepic" style="height: 16px;float: left;width: 16px;">';
					html+='</a></td>';
					var str = aux.Citacao;
					var n = str.length;
					if(n < 40){
						html+='<td id="notiFriendName" >'+aux.NomeUpvoter+ ' just upvoted your quote "' + aux.Citacao + '"</td></tr>';
					}
					else{
						var res = str.substring(0, 40);
						html+='<td id="notiFriendName" >'+aux.NomeUpvoter+ ' just upvoted your quote "' + res + '..."</td></tr>';
					}
					
					
					//removeNotification(aux.IDNotificacao);
					
					}
					html+='</tbody></table><a onclick="'+"closeNoti('upvotes')"+'" id="closeNoti" ><i class="fa fa-times"></i></a>';

				$('.show_notificationsUpvotes').append(html);

				$('#notiTableUpvotes').each(function() {
            var currentPage = 0;
            var numPerPage = 2;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<div class="pager"></div>');
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }
            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            removeNotification(aux.IDNotificacao);
        });

			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});

}

function removeNotification(IDNotificacao){
	$.ajax({             
			type: 'GET',
			url: '../../actions/users/removenotification.php',
			data: 'notif='+IDNotificacao,
			success: function (data) {			
			},
			error: function (xhr, status, error) {
                alert(status);
                alert(xhr.responseText);
            }
	});


}


window.onresize = function() {
    var mleft = $(window).width();
    if ($('#show_notificationsFriends:visible'))
		$('.show_notificationsFriends').css('margin-left', mleft/2-200);
	if ($('#show_notificationsFollowers:visible'))
		$('.show_notificationsFollowers').css('margin-left', mleft/2-200);
	if ($('#show_notificationsUpvotes:visible'))
		$('.show_notificationsUpvotes').css('margin-left', mleft/2-200);
}