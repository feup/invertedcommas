$(document).ready(function() {
	
	  $('#addquoteform').submit(function (event) {
	  			event.preventDefault();
	  			var dataToSend = $("#addquoteform").serialize();
				$.ajax({             
					type: 'get',
					url: '../../actions/quotes/quotes.php',
					data: dataToSend,
					success: function (data) {
					
					 	$('#addquote').val("");
	 					$('#addsource').val("");
	 					profileupdate();
					},
					error: function (xhr, status, error) {
                		alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
    				}

				});
		});
	
});