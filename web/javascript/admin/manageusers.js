  $.ajax({             
    type: 'get',
    url: '../../api/listusers.php',
    success: function (data) {
        var json = JSON.parse(data);
        var userrow = '';
        console.log(json);
        for(var i=10;i>=0;i--)
        {
        	console.log(json[i]);
            userrow += '<tr><td>'+json[i].ID+'</td><td>'+json[i].Nome+'</td><td>'+json[i].TotalQuotes+'</td><td><i class="fa fa-desktop" style="font-size: 18px;"></i></td></tr>';
        }
        $('#recentusers').append(userrow); 	

        $("table").tablesorter({debug: true});
        $('.paginated').each(function() {
            var currentPage = 0;
            var numPerPage = 4;
            var $table = $(this);
            $table.bind('repaginate', function() {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);
            var $pager = $('<div class="pager"></div>');
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }
            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
        });

}


});

 function submitdelete(id)
 {

 		var idcit =  id;
		var dataToSend = "idcitacao=" + idcit;
		$.ajax({             
			type: 'get',
			url: '../../actions/quotes/delete.php',
			data: dataToSend,
			success: function (data) {
				alert('eliminado com sucesso');
				$('#searchadminform').submit();
			},
			error: function (xhr, status, error) {
				alert(status);
				alert(xhr.responseText);
			}
		});
		



 }

 function changeUser(iduser)
 {
    var id = iduser;
    var nome = $('#newname').val();
    var email = $('#newemail').val();

    $.ajax({             
      type: 'GET',
      url: '../../actions/users/adminedit.php?adminid='+id+'&adminusername='+nome+'&adminemail='+email,
      success: function (data) {
        alert('sucess'); 
        $('#searchadminform').submit();

      },
      error: function (xhr, status, error) {
        alert(status);
        alert(xhr.responseText);
      }
    });
 }




  $(document).ready(function() {

  	


      $('#searchadminform').submit(function (event) {
        event.preventDefault();

        var sc = $("#sc").val();
        var sb = $("#sb").val();
        $('#searchresultstable').empty();
        if(sb == 0)
        {
         var dataToSend = 'search_content=' + sc;
         $.ajax({             
            type: 'GET',
            url: '../../actions/admin/adminUserSearch.php',
            data: dataToSend,
            success: function (data) {

                var json = JSON.parse(data);

                if(typeof json['id'] == "undefined") $('#searchresultstable').append('<br><label> Search Results: </label><br>No results found.');
                else {

                    $('#searchresultstable').append('<br><label> Search Results: </label><br><table class="table table-bordered table-hover table-striped tablesorter"><thead><tr><th>User ID</th><th>Username</th><th>Email</th><th>View / Save Changes</i></th></tr></thead><tbody><tr>'+
                        '<td>'+json['id']+'</td>'+
                        '<td><input id="newname" type="text" name="nome" value="'+json['nome']+'"</input></td>'+
                        '<td><input id="newemail" type="text" name="email" value="'+json['email']+'"</input></td>'+
                        '<td><i class="fa fa-desktop" style="font-size: 18px;"></i><a onClick="changeUser('+json['id']+');"><i class="fa fa-wrench" style="font-size: 18px;padding-left: 10px;"></i></a></td>'+
                        '</tr></tbody></table>');
                }
            },
            error: function (xhr, status, error) {
                if(json['id'] == 'undefined') $('#searchresultstable').append('No results found.');
            }
        });
}

       if(sb == 1)
        {
         var dataToSend = 'search_content=' + sc;
         $.ajax({             
            type: 'GET',
            url: '../../actions/admin/adminQuoteSearch.php',
            data: dataToSend,
            success: function (data) {

                var json = JSON.parse(data);

                if(typeof json['id'] == "undefined") $('#searchresultstable').append('<br><label> Search Results: </label><br>No results found.');
                else {

                    $('#searchresultstable').append('<br><label> Search Results: </label><br><table class="table table-bordered table-hover table-striped tablesorter"><thead><tr><th>Quote ID</th><th>Texto</th><th>Fonte</th><th>User ID</th><th>Data</th><th>Remove</th></tr></thead><tbody><tr>'+
                        '<td>'+json['id']+'</td>'+
                        '<td>'+json['textoquote']+'</td>'+
                        '<td>'+json['fonte']+'</td>'+
                        '<td>'+json['iduser']+'</td>'+
                        '<td>'+json['data']+'</td>'+
                        '<td><a onClick="submitdelete('+json['id']+');"><i class="fa fa-trash-o" id="deletequote" style="font-size: 18px;"></i></a></td>'+
                        '</tr></tbody></table>');
                }

            },
            error: function (xhr, status, error) {
                if(json['id'] == 'undefined') $('#searchresultstable').append('No results found.');
            }
        });
}



});

});