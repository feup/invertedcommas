$.getJSON( "https://api.trello.com/1/lists/537d203e9021e83ca95f64ac/cards?key=98f45c258256ba84a3d7a202051f220d", function( data ) {
  var stories ='';
  var d = new Date();

  var month = d.getMonth()+1;
  var day = d.getDate();

  var datenow = d.getFullYear() + '-' +
    ((''+month).length<2 ? '0' : '') + month + '-' +
    ((''+day).length<2 ? '0' : '') + day;

  var daysdiff = 0;
  for(var i=0;i<5;i++)
  {
    var lastdate = ''+data[i].dateLastActivity.substring(0,10).replace('-','/').replace('-','/');
    var daysdiff =  Math.floor(( Date.parse(datenow) - Date.parse(lastdate) ) / 86400000);

  stories += '<a href="'+data[i].url+'" class="list-group-item"><span class="badge">'+daysdiff+' days ago</span><i class="fa fa-user"></i> '+data[i].name+'</a>';
  }
  $('#trellonot').append(stories);

});


$.getJSON( "https://api.trello.com/1/lists/537d2164add1e771ad80dc6c/cards?key=98f45c258256ba84a3d7a202051f220d", function( data ) {
  var stories ='';
  
  var d = new Date();

  var month = d.getMonth()+1;
  var day = d.getDate();

  var datenow = d.getFullYear() + '-' +
    ((''+month).length<2 ? '0' : '') + month + '-' +
    ((''+day).length<2 ? '0' : '') + day;

  var daysdiff = 0;
  for(var i=0;i<5;i++)
  {
    var lastdate = ''+data[i].dateLastActivity.substring(0,10).replace('-','/').replace('-','/');
    var daysdiff =  Math.floor(( Date.parse(datenow) - Date.parse(lastdate) ) / 86400000);

  stories += '<a href="'+data[i].url+'" class="list-group-item"><span class="badge" style="background-color: #17B62A;">'+daysdiff+' days ago</span><i class="fa fa-check"></i> '+data[i].name+'</a>';
  }
  $('#trelloyes').append(stories);

});


