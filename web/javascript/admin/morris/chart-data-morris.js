// First Chart Example - Area Line Chart

Morris.Area({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-area',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
	{ d: '2014-05-01', visits: 802 },
	{ d: '2014-05-02', visits: 783 },
	{ d: '2014-05-03', visits:  820 },
	{ d: '2014-05-04', visits: 839 },
	{ d: '2014-05-05', visits: 792 },
	{ d: '2014-05-06', visits: 859 },
	{ d: '2014-05-07', visits: 790 },
	{ d: '2014-05-08', visits: 1680 },
	{ d: '2014-05-09', visits: 1592 },
	{ d: '2014-05-10', visits: 1420 },
	{ d: '2014-05-11', visits: 882 },
	{ d: '2014-05-12', visits: 889 },
	{ d: '2014-05-13', visits: 819 },
	{ d: '2014-05-14', visits: 849 },
	{ d: '2014-05-15', visits: 870 },
	{ d: '2014-05-16', visits: 1063 },
	{ d: '2014-05-17', visits: 1192 },
	{ d: '2014-05-18', visits: 1224 },
	{ d: '2014-05-19', visits: 1329 },
	{ d: '2014-05-20', visits: 1329 },
	{ d: '2014-05-21', visits: 1239 },
	{ d: '2014-05-22', visits: 1190 },
	{ d: '2014-05-23', visits: 1312 },
	{ d: '2014-05-24', visits: 1293 },
	{ d: '2014-05-25', visits: 1283 },
	{ d: '2014-05-26', visits: 1248 },
	{ d: '2014-05-27', visits: 1323 },
	{ d: '2014-05-28', visits: 1390 },
	{ d: '2014-05-29', visits: 1420 },
	{ d: '2014-05-30', visits: 1529 },
	{ d: '2014-05-31', visits: 1892 },
  ],
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['visits'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Visits'],
  // Disables line smoothing
  smooth: false,
});

Morris.Donut({
  element: 'morris-chart-donut',
  data: [
    {label: "Aggressive", value: 1.01},
    {label: "Manipulative", value: 2.2},
    {label: "Passive", value: 6},
    {label: "Assertive", value: 12.2}
  ],
  formatter: function (y) { return y + " out of 15" ;}
});

Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-line',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
	{ d: '2014-05-01', visits: 802 },
	{ d: '2014-05-02', visits: 783 },
	{ d: '2014-05-03', visits:  820 },
	{ d: '2014-05-04', visits: 839 },
	{ d: '2014-05-05', visits: 792 },
	{ d: '2014-05-06', visits: 859 },
	{ d: '2014-05-07', visits: 790 },
	{ d: '2014-05-08', visits: 1680 },
	{ d: '2014-05-09', visits: 1592 },
	{ d: '2014-05-10', visits: 1420 },
	{ d: '2014-05-11', visits: 882 },
	{ d: '2014-05-12', visits: 889 },
	{ d: '2014-05-13', visits: 819 },
	{ d: '2014-05-14', visits: 849 },
	{ d: '2014-05-15', visits: 870 },
	{ d: '2014-05-16', visits: 1063 },
	{ d: '2014-05-17', visits: 1192 },
	{ d: '2014-05-18', visits: 1224 },
	{ d: '2014-05-19', visits: 1329 },
	{ d: '2014-05-20', visits: 1329 },
	{ d: '2014-05-21', visits: 1239 },
	{ d: '2014-05-22', visits: 1190 },
	{ d: '2014-05-23', visits: 1312 },
	{ d: '2014-05-24', visits: 1293 },
	{ d: '2014-05-25', visits: 1283 },
	{ d: '2014-05-26', visits: 1248 },
	{ d: '2014-05-27', visits: 1323 },
	{ d: '2014-05-28', visits: 1390 },
	{ d: '2014-05-29', visits: 1420 },
	{ d: '2014-05-30', visits: 1529 },
	{ d: '2014-05-31', visits: 1892 },
  ],
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['visits'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Visits'],
  // Disables line smoothing
  smooth: false,
});

Morris.Bar ({
  element: 'morris-chart-bar',
  data: [
	{device: 'iPhone', geekbench: 136},
	{device: 'iPhone 3G', geekbench: 137},
	{device: 'iPhone 3GS', geekbench: 275},
	{device: 'iPhone 4', geekbench: 380},
	{device: 'iPhone 4S', geekbench: 655},
	{device: 'iPhone 5', geekbench: 1571}
  ],
  xkey: 'device',
  ykeys: ['geekbench'],
  labels: ['Geekbench'],
  barRatio: 0.4,
  xLabelAngle: 35,
  hideHover: 'auto'
});
